一个能直接运行的Ninja例子，build.ninja文件怎么写？
===

|作者|将狼才鲸|
|---|---|
|创建日期|2024-03-08|

* [CSDN文章阅读地址](https://blog.csdn.net/qq582880551/article/details/136564417)
* [Gitee源码和工程下载地址](https://gitee.com/langcai1943/OH-NEXT-Kernel/tree/develop/src/standalone/01_ninja)

---

* 如果你使用的是Windows + MSYS2 + MinGW64
  * 已提前安装过gcc
  * msys2命令行中使用 pacman -S mingw-w64-x86_64-ninja 安装Ninja软件
    * 如何搜索msys2中的软件 https://packages.msys2.org/package/mingw-w64-x86_64-ninja?repo=mingw64

```shell
jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel
$ pacman -S mingw-w64-x86_64-ninja
resolving dependencies...
looking for conflicting packages...

Packages (1) mingw-w64-x86_64-ninja-1.11.1-2

Total Download Size:   0.44 MiB
Total Installed Size:  1.73 MiB

:: Proceed with installation? [Y/n] y
:: Retrieving packages...
 mingw-w64-x86_64-ninja-1.1...   449.2 KiB   286 KiB/s 00:02 [###############################] 100%
(1/1) checking keys in keyring                               [###############################] 100%
(1/1) checking package integrity                             [###############################] 100%
(1/1) loading package files                                  [###############################] 100%
(1/1) checking for file conflicts                            [###############################] 100%
(1/1) checking available disk space                          [###############################] 100%
:: Processing package changes...
(1/1) installing mingw-w64-x86_64-ninja                      [###############################] 100%

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel
$ ninja --version
1.11.1

```
* 如果是Ubuntu系统，则用下面命令安装软件：
  * sudo apt install ninja-build

* 安装好的软件在msys2的 /mingw64/bin/ 目录下
* 将这个目录加入到Linux也就是msys2的环境变量中，如果你曾经添加过，则就不要重复添加了
  * ```echo export PATH=$PATH:/mingw64/bin >> ~/.bashrc```	打开家目录下的.bashrc隐藏文件，在最后面加入一行：export PATH=$PATH:/mingw64/bin，保存并关闭文件
  * source ~/.bashrc	立即生效环境变量

* 默认的构建文件为当前目录下的 build.ninja 文件，类似于Linux中的Makefile文件
  * ninja软件只实现最基本的功能，一般需要其它上层软件来生成一个build.ninja文件，例如Gn软件
  * 你也可以手写build.ninja文件，就像你手写Makefile文件一样，但是不建议你这么做，所以你也不用看懂ninja配置文件怎么写，以后还是用Gn工具来生成它，下面我会手写一个简单的配置文件，其语法也和Makefile类似，使用了两个空格代替Makefile的tag键。

* 进入到本仓库的ninja演示目录
  * cd /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja

* 新建一个mian.c文件，写上hello world输出

```c
jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ cat main.c
#include <stdio.h>

int main()
{
        printf("hello world\n");
        fflush(stdout);

        while (1);

        return 0;
}
```

* touch build.ninja	新建一个ninja配置文件，写上编译参数

```shell
jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ cat build.ninja
cc = gcc
cflags = -Wall

rule cc
  command = $cc $cflags -c $in -o $out

build main.o: cc main.c

rule link
  command = $cc $cflags $in -o $out

build demo.exe: link main.o

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
```

* 编译命令为 ninja 类似于make的用法
* 清除上次编译结果的命令为 ninja -t clean

```shell
jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ls
build.ninja  main.c

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ninja
[2/2] gcc -Wall main.o -o demo.exe

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ls
build.ninja  demo.exe  main.c  main.o

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ./demo.exe
hello world


jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ninja -t clean
Cleaning... 2 files.

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ls
build.ninja  main.c

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja

```

* 参考网址：
  * [ninja介绍及使用](https://blog.csdn.net/MAX__YF/article/details/125856258)
  * [ninja使用](https://blog.csdn.net/xyyangkun/article/details/103965554)
  * [Ninja构建系统入门--手敲一遍BUILD.ninja](https://blog.csdn.net/qq_32566003/article/details/120502719)
  * [v61.03 鸿蒙内核源码分析(忍者ninja) ](https://zhuanlan.zhihu.com/p/424402353)
  * [Ninja构建系统入门](https://www.cnblogs.com/sandeepin/p/ninja.html)

