开源鸿蒙NEXT星河版内核嵌入式编程
===

|作者|将狼才鲸|
|---|---|
|创建日期|2024-03-08|

* [CSDN文章阅读地址](https://blog.csdn.net/qq582880551/article/details/136560541)
* [Gitee文章和讲解的Demo下载地址](https://gitee.com/langcai1943/OH-NEXT-Kernel)

---

## 一、前景提要

 * 2024年1月18日，华为放出HarmonyOS NEXT 鸿蒙星河版开发者预览版本（不是HarmonyOS NEXT版，是HarmonyOS NEXT星河版），首次提到用鸿蒙内核（暂命名）取代了Linux内核。
   * 该内核源码还未放出，当前能下载到的开源版本是OpenHarmony4.1的版本，里面的轻型小型系统使用的还是LiteOS内核，标准系统使用的还是Linux内核。
   * 等到纯鸿蒙内核释放后，我会再次更新此文档。
   
 * 开源鸿蒙OpenHarmony和华为HarmonyOS是两样东西！华为鸿蒙里面包含非开源代码。因为很多嵌入式产品单价低、利润薄、出货量大，所以我只会跟踪开源鸿蒙OpenHarmony的鸿蒙内核和底层代码，对华为鸿蒙不涉及。

* 参考网址：
  * [开源鸿蒙系统介绍-OpenHarmony开源项目](https://docs.openharmony.cn/pages/v4.0/zh-cn/OpenHarmony-Overview_zh.md/)
    * [Gitee开源鸿蒙介绍-OpenHarmony Gitee源码仓库](https://gitee.com/openharmony)
  * [开源鸿蒙子系统介绍](https://gitee.com/openharmony/docs/tree/master/zh-cn/readme)
  * [设备开发，系统移植介绍-docs/ zh-cn / device-dev](https://gitee.com/openharmony/docs/tree/OpenHarmony-4.0-Release/zh-cn/device-dev)
  * [下载源码时的顶层仓库-OpenHarmony / manifest](https://gitee.com/openharmony/manifest)
  * [下载源码的4种方法-获取源码](https://www.openharmony.cn/download/)
  * [b站视频教程-OpenHarmony开发者](https://space.bilibili.com/2029471800)
  * [一些开源鸿蒙的板子-学习-开发样例](https://growing.openharmony.cn/mainPlay/sample?titleId=2)
  * [一些开源鸿蒙的产品-兼容性测评结果](https://www.openharmony.cn/armList/)
  * [设备开发的官方教程-学习-学习路径](https://growing.openharmony.cn/mainPlay/learnPath/?developmentRole=2)
  * [开发板和模块开发的官方教程-学习-在线课程-全部课程](https://www.openharmony.cn/courses/detail?titleId=1&newDate=1)

## 二、OpenHarmony源码下载、编译与运行

### 1、源码整体感知

* 这是OpenHarmony的源码目录：

```shell
jim@ubuntu:~/OpenHarmony$ ls -a
.             build          developtools  foundation     .repo
..            build.py       device        .gn            test
applications  build.sh       docs          kernel         third_party
base          commonlibrary  drivers       productdefine  vendor

159,898 items, totalling 11.6 GB

当前下载的是单一芯片单一系统类型的代码，总量11G，如果是全量代码，OpenHarmony4.0会有30G。
```

* OpenHarmony有Gitee源码仓库：https://gitee.com/openharmony ，但是和其它简单的Git仓库不一样，它不是通过 git clone xxx 一个命令就能下载全量代码的，而是和安卓类似，需要用到manifest这个顶层仓库，并且通过repo工具把很多个Git仓库的特定版本拉到本地同一个目录。
  * 顶层目录下没有.git的文件夹，只有.repo的文件夹，而底下的每个文件夹可能就是单独的Git仓库，有自己的.git隐藏文件夹
* 参考网址：
  * [官方文档-获取源码](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/get-code/sourcecode-acquire.md)
  * [通过特定命令单独获取OpenHarmony标准、轻量、小型系统指定开发板的代码](https://blog.csdn.net/weixin_58069108/article/details/132926287)
  * [华为云14天鸿蒙设备开发-Day1源码获取](https://blog.csdn.net/kouqi627/article/details/125778066)  
  * [使用码云（Gitee）获取开源鸿蒙+欧拉系统源代码](https://blog.csdn.net/qq_42450767/article/details/122153660)  
  * [OpenHarmony-v4.1-beta1.md ](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v4.1-beta1.md) 
  * [海思（二）OpenHarmony获取源码  ](https://blog.csdn.net/weixin_45768644/article/details/124243645)
  * [git repo工具详细使用教程——彻底学会Android repo的使用](https://blog.csdn.net/ljz0929/article/details/122928242)  
  * [如果获取鸿蒙源代码？](https://www.zhihu.com/question/464072336/answer/2537982553?utm_id=0)

* 特定的开发板介绍：
  * [OpenHarmony开发板列表，官方支持的所有开发板和芯片](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/dev-board-on-the-master.md)
  * [轻量、小型、标准系统的区别-快速入门概述](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-overview.md)
  * [OpenHarmony / docs 各个子系统介绍](https://gitee.com/openharmony/docs/tree/master/zh-cn/readme)
* 官方的轻型系统开发板中我中意的有：瑞芯微RK2206和STM32F407IGT6，信息如下
  * [OpenHarmony / device_soc_rockchip](https://gitee.com/openharmony/device_soc_rockchip)
  * [OpenHarmony / device_board_lockzhiner](https://gitee.com/openharmony/device_board_lockzhiner)
  * [OpenHarmony / vendor_lockzhiner](https://gitee.com/openharmony/vendor_lockzhiner)

* 下载源码时可以指定目标硬件，开源鸿蒙的源码下载步骤和安卓类似，使用repo工具和https://gitee.com/openharmony/manifest 这个仓库，manifet是组织了很多独立的git仓库，一起给你按tag拉出一份全量代码；
  * 从manifest仓库你们的readme可知，我下载时可以指明参数，例如 ohos:mini 轻型系统，ohos:chipset 选择一款芯片
  * repo init -u URL -b master 下载整个系统全量代码，30多G
  * repo init -u URL -b master -g ohos:mini 下载轻量系统全量代码。
  * repo init -u URL -b master -m chipsets/chipsetN.xml -g ohos:mini 下载轻量系统指定芯片的代码。
  * 在 https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/dev-board-on-the-master.md 里面有支持的所有开发板和芯片，我选中了ARM Cortex-M内核的Niobe407开发板的STM32F407IGT6芯片，小凌派-RK2206开发板的RK2206，还有使用QEMU虚拟机的虚拟开发板；
  * 它们的型号和配置是 https://gitee.com/openharmony/manifest/blob/master/chipsets/niobe407/niobe407.xml 、 https://gitee.com/openharmony/manifest/blob/master/chipsets/lockzhiner/lingpi.xml 、 https://gitee.com/openharmony/manifest/blob/master/chipsets/qemu/qemu.xml

### 2、源码下载

#### 2.1 最简单的下载方式

* 推荐你下载OpenHarmony4.1全量代码，最简单，直接网址下载，总共30G左右，不用解决Linux环境下的各种依赖错误。
  * 下载网址 https://repo.huaweicloud.com/openharmony/os/4.1-Beta1/code-v4.1-Beta1.tar.gz
  * 出了新版本后也是进入上面类似的网址，进入新版本的文件夹种下载压缩包即可。

#### 2.2 常规的下载方式

* 如果你有过Linux内核开发经验，知道接下来要使用OpenHarmony在哪款板子上运行，则建议你下载特定类型（轻型小型标准）、特定芯片的系统，此下载步骤需要在Linux发行版如Ubuntu下进行，总共11G左右；
  * 安装VMware或者VMware-Player或者Virual Box虚拟机，初学者建议使用VMware，网上教程更多，过程略；
  * 安装Ubuntu系统，过程略，我使用的是Ubuntu18.04.6；
  * 执行命令：

```shell
	推荐使用Ubuntu18.04及以上版本，Ubuntu16.04不行，Windows + MSYS2 + MinGW64不行；
	安装Ubuntu后把“电源”和“隐私”的设置都改成永远不锁屏，否则一锁屏后下载就会停住，需要退出重新来，容易导致文件冲突；
	建议在你创建好的文件夹内打开命令行，如果你在~家目录打开命令行，则下载的源码文件都在家目录下，会和其它文件混在一起，文件夹很多，分辨不出哪个是系统原有的，想再剪切到别的文件就困难了。
	进入Ubuntu系统后，在桌面上按鼠标右键，然后点击打开终端这一项，老系统可以按Ctrl + Alt + T打开命令行终端，默认是在~家目录
	mkdir ~/openHarmony			/* 家目录下新建文件夹 */
	cd ~/openHarmony			/* 进入新建的文件夹 */
	sudo apt-get install git	/* 安装Git软件 */
	git config --global user.name jimXXX	/* 配置Git，输入你自己的名字 */
	git config --global user.email jimXXX@163.com	/* 配置Git，输入你自己的邮箱 */
	git config --global credential.helper store	/* 配置Git为自动保存账号密码 */
	ssh-keygen -t rsa -C jimXXX@163.com	/* 生成Gitee网站要用到的密钥，这里是输入你自己的邮箱，回车三次 */
	gedit ~/.ssh/id_rsa.pub /* 复制密钥，复制里面全部的文本内容 */
	打开网址 https://gitee.com/ 注册账号，点击齿轮状的设置，点击安全设置中的SSH公钥，将刚刚复制的内容填入到“公钥”这个文本框内，点击确定，输入密码，即添加完Git公钥
```

```shell
	mkdir ~/bin
	sudo apt install curl
	curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 -o ~/bin/repo 
	chmod a+x ~/bin/repo
	sudo apt install python3-pip
		如果用了Ubuntu老版本系统，如Ubuntu18.04，则还要继续执行下面步骤：
        sudo apt-get install python3
        echo alias python=python3 >> ~/.bashrc
		source ~/.bashrc
		python3 --version
		whereis python3
		sudo ln -s /usr/bin/python3 /usr/bin/python

	pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
		如果使用了Ubuntu16.04及以前的系统，这条命令可能会报错，需要你自己解决
	
	gedit ~/.bashrc  打开桌面~下的.bashrc隐藏文件夹
	在最后面加上一句 export PATH=~/bin:$PATH，保存退出
	source ~/.bashrc 生效环境变量
	echo $PATH 查看环境变量
	repo init -u https://gitee.com/openharmony/manifest -b master -m chipsets/niobe407.xml -g ohos:mini 做好下载指定开发板指定芯片源码的准备，我这里是下载ARM Cortex-M内核的Niobe407开发板的STM32F407IGT6芯片
		如果下载全量代码，则使用 repo init -u git@github.com:openharmony/manifest.git -b master --no-repo-verify
		如果下载QEMU模拟器的代码，则使用 repo init -u https://gitee.com/openharmony/manifest -b master -m chipsets/qemu.xml -g ohos:mini

	repo sync -c 开始下载，等待时间会比较久
		需要设置电脑不待机，如果中途卡住，可以Ctrl + C退出当前命令，并再次repo sync -c继续下载
		如果继续下载有冲突，则手动删除有冲突的文件夹后继续使用repo sync -c
		你在哪个文件夹路径使用repo init，就会在这个路径形成一个.repo隐藏文件夹，已经下载完的文件夹会在当前目录中出现
	sudo apt-get install git-lfs
	repo forall -c 'git lfs pull'
		继续拉取大文件
```

* 下载完之后的文件结构如下：

```shell
jim@ubuntu:~/OpenHarmony$ ls -a
.             build          developtools  foundation     .repo
..            build.py       device        .gn            test
applications  build.sh       docs          kernel         third_party
base          commonlibrary  drivers       productdefine  vendor

159,898 items, totalling 11.6 GB

当前下载的是单一芯片单一系统类型的代码，总量11G，如果是全量代码，OpenHarmony4.0会有30G。
```

* repo仓库是由多个git仓库拼起来的，源码根目录下是.repo隐藏文件夹，里面的某些子文件夹下如果有.git隐藏文件夹，那么它就是一个独立的git仓库

### 3、编译系统，生成OHOS_Image可执行文件

* 编译OpenHarmony源码前先下载源码
  * 步骤见：[开源鸿蒙OpenHarmony niobe407 STM32F407IGT6芯片轻型系统全量源码4.1版本下载流程](https://blog.csdn.net/qq582880551/article/details/136330246)
* 确定你要编译哪一块开发板，我这里使用QEMU模拟器虚拟开发板：
  * [可以编译的开发板和芯片列表，含QEMU模拟器 - 编译形态整体说明](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-appendix-compiledform.md/)
  * QEMU模拟器 + ARM Coterx-M核 + mini轻型系统的配置有：
    * qemu_mini_system_demo	arm_mps2_an386	qemu	liteos_m	mini	arm-cortex-m4
    * qemu_cm55_mini_system_demo	arm_mps3_an547	qemu	liteos_m	mini	arm-cortex-m55
    * [Qemu Arm Cortex-m4 mps2-an386 教程](https://gitee.com/openharmony/device_qemu/blob/master/arm_mps2_an386/README_zh.md)
    * [Qemu Arm Cortex-m55 mps3-an547 教程](https://gitee.com/openharmony/device_qemu/blob/master/arm_mps3_an547/README_zh.md)
    * [QEMU（Quick Emulator）鸿蒙QEMU模拟器操作指南](https://gitee.com/openharmony/device_qemu/tree/master)
* 下载：安装VMware虚拟机 + Ubuntu发行版系统 + Git软件 + repo软件 + Python
  * 如果你之前不是下载的全量30G代码，是下载的别的开发板的代码，那么在下载的openHarmony目录下再执行一次下面的命令，多下一份qemu模拟器的源码；
  * 使用 repo init -u https://gitee.com/openharmony/manifest -b master -m chipsets/qemu.xml -g ohos:mini 初始化对应于QEMU模拟器（类似于开发板）的全套源码
  * 按之前的的步骤接着下载鸿蒙源码
    * repo sync -c
    * repo forall -c 'git lfs pull'

#### 3.1 最简单的编译流程，使用Docker

* 使用鸿蒙官方已经准备好的Docker环境
  * [Docker编译环境](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/get-code/gettools-acquire.md/)
  * sudo apt install docker.io
  * sudo docker pull swr.cn-south-1.myhuaweicloud.com/openharmony-docker/docker_oh_mini:3.2
  * cd ~/openHarmony  进入到你下载的开源鸿蒙源码的路径，再执行下面这一条
  * docker run -it -v $(pwd):/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/docker_oh_mini:3.2  
  * 这时已经进入到docker的虚拟环境

```shell
jim@ubuntu:~/openHarmony$ sudo docker run -it -v $(pwd):/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/docker_oh_mini:3.2
root@949f932a2f80:/home/openharmony# ls
applications  build     build.sh       developtools  docs     foundation  ohos_config.json  prebuilts      qemu-run  third_party
base          build.py  commonlibrary  device        drivers  kernel      out               productdefine  test      vendor
root@949f932a2f80:/home/openharmony#
```

* python3 build.py -p qemu_mini_system_demo@ohemu
  * 你要编译什么板子，就使用什么命令，我这里是编译qemu的轻型系统
  * 开始编译轻型系统，编译时间会需要很久，十几分钟……
  * 编译工具分为轻型、小型、标准，而芯片、开发板的归属也分为这三种，下载的工具和你编译的参数必须属于同一种类，否则会编译各种报错！！！
* 在编译结束后，编译所生成的文件都会被存放在out/{device_name}/目录下，结果镜像输出在out/{device_name}/packages/phone/images/目录下。
* 如果遇到报错，则解决报错

* 编译成功后是这样的打印：

```shell
[OHOS INFO] [1614/1615] STAMP obj/build/ohos/images/make_images.stamp
[OHOS INFO] [1615/1615] STAMP obj/build/core/gn/images.stamp
[OHOS INFO] ccache_dir = /root/.ccache, ccache_exec = /usr/bin/ccache
[OHOS INFO] --------------------------------------------
[OHOS INFO] ccache summary:
[OHOS INFO] ccache version: 3.7.7
[OHOS INFO] cache hit (direct): 0
[OHOS INFO] cache hit (preprocessed): 0
[OHOS INFO] cache miss: 0
[OHOS INFO] hit rate: 0.00% 
[OHOS INFO] miss rate: 0.00% 
[OHOS INFO] Cache size (GB): 
[OHOS INFO] ---------------------------------------------
[OHOS INFO] c targets overlap rate statistics
[OHOS INFO] subsystem       	files NO.	percentage	builds NO.	percentage	overlap rate
[OHOS INFO] hiviewdfx       	      12	0.8%	      12	0.8%	1.00
[OHOS INFO] kernel          	     902	63.5%	     902	63.5%	1.00
[OHOS INFO] security        	      61	4.3%	      61	4.3%	1.00
[OHOS INFO] startup         	      26	1.8%	      26	1.8%	1.00
[OHOS INFO] systemabilitymgr	      15	1.1%	      15	1.1%	1.00
[OHOS INFO] thirdparty      	     393	27.7%	     393	27.7%	1.00
[OHOS INFO] 
[OHOS INFO] c overall build overlap rate: 1.00
[OHOS INFO] 
[OHOS INFO] 
[OHOS INFO] qemu_mini_system_demo@ohemu build success
[OHOS INFO] Cost time:  0:05:47
root@ff38bf1e3e75:/home/openharmony# 

jim@ubuntu:~/openHarmony/out/arm_mps2_an386/qemu_mini_system_demo$ ls
all_parts_host.json           build_configs   config.h         kconfig_files.txt  OHOS_Image.bin              src_installed_parts.json
all_parts_info.json           build.log       error.log        libs               OHOS_Image.map              src_sa_infos_tmp.json
args.gn                       build.ninja     etc              NOTICE_FILES       OHOS_Image.sym.sorted       startup
binary_installed_parts.json   build.ninja.d   gen              obj                packages                    thirdparty
build.1709259296.3884952.log  build.trace.gz  hiviewdfx        OHOS_Image         security                    toolchain.ninja
build.1709259767.6772568.log  config.gni      kconfig_env.txt  OHOS_Image.asm     sorted_action_duration.txt
jim@ubuntu:~/openHarmony/out/arm_mps2_an386/qemu_mini_system_demo$ 
```

* 生成的系统镜像image是这里面的OHOS_Image文件

* 如需退出Docker，执行exit命令即可。这个命令会停止当前的Docker容器，并返回到您的操作系统。
* 下一步就可以把编译好的操作系统在QEMU模拟器中运行起来啦。

#### 3.2 使用ohos-build工具编译

* 最常用的编译方式是使用hb命令，也就是ohos-build工具，这是鸿蒙自己的Python工具
  * [了解OpenHarmony-编译](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-pkg-3568-build.md/)
  * OpenHarmony支持hb和build.sh两种编译方式。
  * [安装hb编译工具](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-pkg-install-tool.md)
  * [安装库和工具集](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-pkg-install-package.md)
  * [.sh方式编译构建指导](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/subsystems/subsys-build-all.md)
	
  * python3 -V  查看python版本，如果是3.8以下，则：
    * sudo apt install python3.8
	* sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 1
	* sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2
	* sudo update-alternatives --config python3
	* python3 -V
  
  * python3 -m pip install --user ohos-build
    * pip3是Python的包管理工具，用于安装、升和管理Python包
	* 需要python3.8以上的版本
* 安装流程介绍：
  * [hb安装异常处理](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-pkg-common-hberr.md/)  
  * [快速入门-如何编译程序](https://gitee.com/openharmony/docs/tree/master/zh-cn/device-dev/quick-start)
  * [安装库和工具集-编译流程](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-pkg-install-package.md)
* 如果你是Ubuntu18.04，则先执行
  * cd /usr/lib/python3/dist-packages
  * cp apt_pkg.cpython-36m-x86_64-linux-gnu.so apt_pkg.so apt_pkg.cpython-39m-x86_64-linux-gnu.so apt_pkg.so
  * cd -
* 再安装依赖库

```shell
sudo apt-get update && sudo apt-get install binutils binutils-dev git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib gcc-arm-linux-gnueabi libc6-dev-i386 libc6-dev-amd64 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby genext2fs device-tree-compiler make libffi-dev e2fsprogs pkg-config perl openssl libssl-dev libelf-dev libdwarf-dev u-boot-tools mtd-utils cpio doxygen liblz4-tool openjdk-8-jre gcc g++ texinfo dosfstools mtools default-jre default-jdk libncurses5 apt-utils wget scons python3.8-distutils tar rsync git-core libxml2-dev lib32z-dev grsync xxd libglib2.0-dev libpixman-1-dev kmod jfsutils reiserfsprogs xfsprogs squashfs-tools pcmciautils quota ppp libtinfo-dev libtinfo5 libncurses5-dev libncursesw5 libstdc++6 gcc-arm-none-eabi vim ssh locales libxinerama-dev libxcursor-dev libxrandr-dev libxi-dev
```

* 仔细看安装后的信息，如果提示有哪些包未安装成功，则你需要单独安装
  * ```sudo apt-get install g++-multilib```
  * sudo apt-get install gcc-arm-linux-gnueabi
  * sudo apt-get install gcc-multilib
  * sudo apt-get install lib32ncurses5-dev
  * sudo apt-get install lib32z1-dev
  * sudo apt-get install libc6-dev-i386
  * sudo apt-get install gcc-7-arm-linux-gnueabi
  * sudo apt-get install libc6-amd64:i386
  * sudo aptitude  install gcc-multilib:i386  使用aptitude命令可以自动解决包依赖关系，需要先安装aptitude软件

* 已经按前面的流程下载好源码
* cd ~/openHarmony  进入到你下载的源码根目录
* bash build/prebuilts_download.sh  安装和芯片相关的编译器和二进制工具，全自动的，会下载的同时进行安装，要等待一段时间，有进度条
* python3 -m pip install --user build/hb
* gedit ~/.bashrc
* 将 export PATH=~/.local/bin:$PATH 加入到最后一行并保存退出
* source ~/.bashrc
* hb --help    这个命令执行时可能有报错，有什么错误就解决什么错误，例如用下面方法解决报错
  * python3 -m pip uninstall ohos-build
  * python3 -m pip install --user ohos-build==0.4.3
* hb set  进行编译设置
  * 第一步目录输入.  选择源码根目录的当前目录
  * 第二步选择开发板，我使用上下方向键选择qemu_mini_system_demo这款qemu的模拟器开发板，ARM Cortex-M4内核的虚拟芯片
    * [编译形态整体说明，含QEMU，可以编译哪些开发板和芯片](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-appendix-compiledform.md/)
    * [Qemu Arm Cortex-m4 mps2-an386 教程](https://gitee.com/openharmony/device_qemu/blob/master/arm_mps2_an386/README_zh.md)
    * qemu_mini_system_demo	arm_mps2_an386	qemu	liteos_m	mini	arm-cortex-m4

```shell
jim@ubuntu:~/openHarmony$ hb set
[OHOS INFO] Input code path: .
OHOS Which product do you need?  qemu_mini_system_demo
```

* hb build -f  开始编译，有报错则处理报错
* 出现build success表示编程成功，然后进行烧录，或者直接用QEMU模拟器进行运行。

* 参考资料：
  * [基于OpenHarmony搭建的Qemu仿真实验环境](https://blog.csdn.net/qazwsx157/article/details/128662734)  
  * [ohos-build 安装失败分析](https://blog.csdn.net/qq_45396672/article/details/123186586)
  * [基于Ubuntu20.04搭建OpenHarmony v3.0.6的qemu仿真环境](https://blog.csdn.net/a171232886/article/details/128716627)
  * [基于OpenHarmony搭建的Qemu仿真实验环境](https://blog.csdn.net/qazwsx157/article/details/128662734)
  * [搭建一个鸿蒙运行环境，来一窥神秘鸿蒙](https://www.ofweek.com/ai/2021-05/ART-201715-8900-30501256_3.html)

#### 3.3 使用传统Linux脚本方式编译

* 如果你用hb的方式编译始终出错，那么可以试试使用传统.sh脚本的方式
  * [使用build.sh脚本编译源码](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-pkg-common-build.md)
* ./build.sh --product-name qemu_mini_system_demo --ccache	/* 需要换成你自己开发板的名称，我这里使用的是QEMU模拟器的虚拟开发板 */
  * 有报错则解决报错
* 检查编译结果。编译完成后，log中显示如下：
  * post_process
  * =====build name successful.
  * 编译所生成的文件都归档在out/{device_name}/目录下，结果镜像输出在out/{device_name}/packages/phone/images/ 目录下。

* 如果依然编译有报错，那还可以试试Docker
* 如果你只是想编译应用程序，那么可以使用DevEco Device Tool
  * [应用编译](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-ide-3861-build.md)

### 3、系统运行

* 如果你是在某一块开发板上运行开源鸿蒙系统，那么请参照该开发板对应的sdk文档
* 我准备在QEMU模拟器中运行ARM Cortex-M4的轻型开源鸿蒙系统
  * [官方支持的开发板和模拟器种类-编译形态整体说明](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-appendix-compiledform.md/)
  * 已支持的示例工程，Qemu模拟器: arm_mps2_an386、esp32、riscv32_virt、SmartL_E802
  * [Qemu Arm Cortex-m4 mps2-an386 教程](https://gitee.com/openharmony/device_qemu/blob/HEAD/arm_mps2_an386/README_zh.md)
* 在电脑上安装QEMU模拟器软件，可以在Windows下安装，也可以在Linux发行版下安装
  * [Qemu安装-QEMU Quick Emulator](https://gitee.com/openharmony/device_qemu)  QEMU在Ubuntu下的安装流程；是下载源码后再编译，编译速度很慢，可以make只编译ARM部分的，这样编译速度会快很多
    * ../configure --target-list=arm-softmmu,arm-linux-user
	* make -j4
	* sudo make install
  * 在Windows下的安装流程更简单
  * [Windows的所有安装包](https://qemu.weilnetz.de/w64/) 安装6.2.0及以上的版本都可以
  * 官网下载很慢，可以用国内网站下载 https://www.mydown.com/soft/172/726002172.shtml
  * Windows下安装后需要手动配置好QEMU的环境变量，或者直接进入到D:\Program Files\qemu的目录下打开CMD命令行界面，这样才能调用qemu的命令
* Ubuntu回到OpenHarmony源码根目录下，使用命令行用QEMU模拟器运行开源鸿蒙系统
  * ./qemu-run --help
  * ./qemu-run -e out/arm_mps2_an386/qemu_mini_system_demo/OHOS_Image
  * qemu-run是个脚本，可以用文本文件打开，它里面也是调用的qemu-system-arm这个命令，只是自动传入了需要的参数
* 这是开源鸿蒙在QEMU硬件模拟器中运行的结果，当前源码中对QEMU Cortex-M4的适配有点问题，会有程序跑飞的报错，你可以再尝试一下别的QEMU开发板，或者以后用OpenHarmony新版本的程序再试一次：


```shell
jim@ubuntu:~/openHarmony$ ./qemu-run -e out/arm_mps2_an386/qemu_mini_system_demo/OHOS_Image
board: arm_mps2_an386

Enter to start qemu[y/n]:entering kernel init...
LfsLowLevelInit: DiskPartition succeed
LfsLowLevelInit: PartitionFormat succeed
LfsLowLevelInit: mount fs on '/littlefs' succeed
LfsLowLevelInit: mkdir '/littlefs' succeed
tcpip_init start
*************Exception Information**************
Type      = 11
ThrdPid   = 25
Phase     = exc in task
FaultAddr = 0xabababab
Current task info:
Task name = (null)
Task ID   = 25
Task SP   = (nil)
Task ST   = 0x0
Task SS   = 0x0
Exception reg dump:
PC        = 0x2100e62a
LR        = 0x2101b045
SP        = 0x210b18e0
R0        = 0x210a725c
R1        = 0x200
R2        = 0x210a725c
R3        = 0x2108a9b3
R4        = 0x0
R5        = 0x0
R6        = 0x0
R7        = 0x210b18e8
R8        = 0x0
R9        = 0x0
R10       = 0x0
R11       = 0x0
R12       = 0xc8000000
PriMask   = 0x0
xPSR      = 0x610f0000
----- backtrace start -----
backtrace 0 -- lr = 0x21015178
backtrace 1 -- lr = 0x2101b044
backtrace 2 -- lr = 0x210235da
backtrace 3 -- lr = 0x210235e2
backtrace 4 -- lr = 0x2102379a
----- backtrace end -----
qemu: fatal: Lockup: can't escalate 3 to HardFault (current priority -1)

R00=210b2ad0 R01=2109fa3f R02=210a18f8 R03=6c0da998
R04=00000000 R05=00000060 R06=00000018 R07=210b1708
R08=00000019 R09=000003e8 R10=218ba1e8 R11=00000000
R12=00000000 R13=210b1708 R14=210011db R15=21000fcc
XPSR=21030006 --C- T handler
s00=00000000 s01=00000000 d00=0000000000000000
s02=00000000 s03=00000000 d01=0000000000000000
s04=00000000 s05=00000000 d02=0000000000000000
s06=00000000 s07=00000000 d03=0000000000000000
s08=00000000 s09=00000000 d04=0000000000000000
s10=00000000 s11=00000000 d05=0000000000000000
s12=00000000 s13=00000000 d06=0000000000000000
s14=00000000 s15=00000000 d07=0000000000000000
s16=00000000 s17=00000000 d08=0000000000000000
s18=00000000 s19=00000000 d09=0000000000000000
s20=00000000 s21=00000000 d10=0000000000000000
s22=00000000 s23=00000000 d11=0000000000000000
s24=00000000 s25=00000000 d12=0000000000000000
s26=00000000 s27=00000000 d13=0000000000000000
s28=00000000 s29=00000000 d14=0000000000000000
s30=00000000 s31=00000000 d15=0000000000000000
FPSCR: 00000000
/home/jim/openHarmony/vendor/ohemu/qemu_mini_system_demo/qemu_run.sh: line 95: 50440 Aborted                 (core dumped) qemu-system-arm -M mps2-an386 -m 16M -kernel $elf_file $qemu_option -append "root=dev/vda or console=ttyS0" -nographic
jim@ubuntu:~/openHarmony$ 
```

* 在Windows的QEMU下可以用这个命令
  * qemu-system-arm -M mps2-an386 -cpu cortex-m4 -kernel OHOS_Image -serial stdio -nodefaults -nographic
  * 另外一个可供你参考的命令 qemu-system-arm -machine versatileab -cpu cortex-a9 -nographic -monitor null -semihosting -append 'some program arguments' -kernel program.axf

```shell
D:\Program Files\qemu>qemu-system-arm -M mps2-an386 -cpu cortex-m4 -kernel OHOS_Image -serial stdio -nodefaults -nographic
qemu-system-arm: warning: nic lan9118.0 has no peer
entering kernel init...
LfsLowLevelInit: DiskPartition succeed
LfsLowLevelInit: PartitionFormat succeed
LfsLowLevelInit: mount fs on '/littlefs' succeed
LfsLowLevelInit: mkdir '/littlefs' succeed
tcpip_init start
*************Exception Information**************
Type      = 11
ThrdPid   = 25
Phase     = exc in task
FaultAddr = 0xabababab
Current task info:
Task name = (null)
Task ID   = 25
Task SP   = (nil)
Task ST   = 0x0
Task SS   = 0x0
Exception reg dump:
PC        = 0x2100e62a
LR        = 0x2101b045
SP        = 0x210b18e0
R0        = 0x210a725c
R1        = 0x200
R2        = 0x210a725c
R3        = 0x2108a9b3
R4        = 0x0
R5        = 0x0
R6        = 0x0
R7        = 0x210b18e8
R8        = 0x0
R9        = 0x0
R10       = 0x0
R11       = 0x0
R12       = 0xc8000000
PriMask   = 0x0
xPSR      = 0x610f0000
----- backtrace start -----
backtrace 0 -- lr = 0x21015178
backtrace 1 -- lr = 0x2101b044
backtrace 2 -- lr = 0x210235da
backtrace 3 -- lr = 0x210235e2
backtrace 4 -- lr = 0x2102379a
----- backtrace end -----
qemu: fatal: Lockup: can't escalate 3 to HardFault (current priority -1)

R00=210b2ad0 R01=2109fa3f R02=210a18f8 R03=6c0da998
R04=00000000 R05=00000060 R06=00000018 R07=210b1708
R08=00000019 R09=000003e8 R10=218ba1e8 R11=00000000
R12=00000000 R13=210b1708 R14=210011db R15=21000fcc
XPSR=21030006 --C- T handler
s00=00000000 s01=00000000 d00=0000000000000000
s02=00000000 s03=00000000 d01=0000000000000000
s04=00000000 s05=00000000 d02=0000000000000000
s06=00000000 s07=00000000 d03=0000000000000000
s08=00000000 s09=00000000 d04=0000000000000000
s10=00000000 s11=00000000 d05=0000000000000000
s12=00000000 s13=00000000 d06=0000000000000000
s14=00000000 s15=00000000 d07=0000000000000000
s16=00000000 s17=00000000 d08=0000000000000000
s18=00000000 s19=00000000 d09=0000000000000000
s20=00000000 s21=00000000 d10=0000000000000000
s22=00000000 s23=00000000 d11=0000000000000000
s24=00000000 s25=00000000 d12=0000000000000000
s26=00000000 s27=00000000 d13=0000000000000000
s28=00000000 s29=00000000 d14=0000000000000000
s30=00000000 s31=00000000 d15=0000000000000000
FPSCR: 00000000

D:\Program Files\qemu>
```

## 三、源码讲解

* 这是下载好的源码文件夹：

```shell
jim@ubuntu:~/openHarmony$ tree -a -L 1
.
├── applications
├── base
├── build
├── build.py -> build/build_scripts/build.py
├── build.sh -> build/build_scripts/build.sh
├── commonlibrary
├── developtools
├── device
├── docs
├── drivers
├── foundation
├── .gn -> build/core/gn/dotfile.gn
├── kernel
├── ohos_config.json
├── out
├── prebuilts
├── productdefine
├── qemu
├── qemu-run -> vendor/ohemu/common/qemu-run
├── .repo
├── test
├── third_party
└── vendor

18 directories, 5 files
jim@ubuntu:~/openHarmony$ tree -a -L 2
.
├── applications
│   └── sample
├── base
│   ├── global
│   ├── hiviewdfx
│   ├── iothardware
│   ├── powermgr
│   ├── security
│   ├── sensors
│   ├── startup
│   └── update
├── build
│   ├── build_scripts
│   ├── bundle.json
│   ├── common
│   ├── compile_env_allowlist.json
│   ├── compile_standard_whitelist.json
│   ├── component_compilation_whitelist.json
│   ├── config
│   ├── core
│   ├── docs
│   ├── .git
│   ├── .gitattributes
│   ├── .gitee
│   ├── .gitignore
│   ├── gn_helpers.py
│   ├── hb
│   ├── LICENSE
│   ├── lite
│   ├── misc
│   ├── OAT.xml
│   ├── ohos
│   ├── ohos.gni
│   ├── ohos_system.prop
│   ├── ohos_var.gni
│   ├── prebuilts_download_config.json
│   ├── prebuilts_download.py
│   ├── prebuilts_download.sh
│   ├── print_python_deps.py
│   ├── __pycache__
│   ├── README_zh.md
│   ├── rust
│   ├── scripts
│   ├── subsystem_compoents_whitelist.json
│   ├── subsystem_config_example.json
│   ├── subsystem_config.json
│   ├── templates
│   ├── test
│   ├── test.gni
│   ├── toolchain
│   ├── tools
│   ├── version.gni
│   └── zip.py
├── build.py -> build/build_scripts/build.py
├── build.sh -> build/build_scripts/build.sh
├── commonlibrary
│   ├── c_utils
│   └── utils_lite
├── developtools
│   ├── global_resource_tool
│   ├── integration_verification
│   ├── packing_tool
│   └── syscap_codec
├── device
│   └── qemu
├── docs
│   ├── CODEOWNERS
│   ├── DCO.txt
│   ├── docker
│   ├── en
│   ├── .git
│   ├── .gitattributes
│   ├── .gitignore
│   ├── image.png
│   ├── LICENSE
│   ├── OAT.xml
│   ├── README.md
│   ├── README_zh.md
│   └── zh-cn
├── drivers
│   ├── hdf_core
│   ├── liteos
│   └── peripheral
├── foundation
│   ├── ability
│   ├── ai
│   ├── arkui
│   ├── bundlemanager
│   ├── communication
│   ├── distributedhardware
│   ├── graphic
│   ├── multimedia
│   ├── systemabilitymgr
│   └── window
├── .gn -> build/core/gn/dotfile.gn
├── kernel
│   ├── liteos_a
│   ├── liteos_m
│   └── uniproton
├── ohos_config.json
├── out
│   ├── arm_mps2_an386
│   ├── arm_virt
│   ├── hb_args
│   ├── ohos_config.json
│   ├── preloader
│   └── sdk
├── prebuilts
│   ├── ark_tools
│   ├── build-tools
│   ├── clang
│   ├── cmake
│   ├── develop_tools
│   ├── gcc
│   ├── mingw-w64
│   ├── packing_tool
│   ├── python
│   └── rustc
├── productdefine
│   └── common
├── qemu
│   ├── qemu-6.2.0
│   └── qemu-6.2.0.tar.xz
├── qemu-run -> vendor/ohemu/common/qemu-run
├── .repo
│   ├── manifests
│   ├── manifests.git
│   ├── manifest.xml
│   ├── project.list
│   ├── project-objects
│   ├── projects
│   ├── repo
│   └── .repo_fetchtimes.json
├── test
│   ├── ostest
│   ├── testfwk
│   └── xts
├── third_party
│   ├── bounds_checking_function
│   ├── cJSON
│   ├── cmsis
│   ├── curl
│   ├── FatFs
│   ├── ffmpeg
│   ├── FreeBSD
│   ├── freetype
│   ├── glslang
│   ├── gn
│   ├── googletest
│   ├── harfbuzz
│   ├── jerryscript
│   ├── jinja2
│   ├── json
│   ├── libjpeg-turbo
│   ├── libpng
│   ├── libunwind
│   ├── littlefs
│   ├── ltp
│   ├── lwip
│   ├── lzma
│   ├── markupsafe
│   ├── mbedtls
│   ├── mksh
│   ├── mtdev
│   ├── musl
│   ├── nghttp2
│   ├── NuttX
│   ├── openssl
│   ├── optimized-routines
│   ├── popt
│   ├── PyYAML
│   ├── qrcodegen
│   ├── spirv-headers
│   ├── spirv-tools
│   ├── sqlite
│   ├── toybox
│   ├── unity
│   ├── vk-gl-cts
│   ├── wpa_supplicant
│   └── zlib
└── vendor
    └── ohemu

140 directories, 42 files
jim@ubuntu:~/openHarmony$ 

```

* 本来应该从开源鸿蒙上电运行的第一行代码开始讲起（也就是复位中断处理的第一条汇编跳转函数），但每块板子的boot都不一样，这是由编译配置决定的，所以我先说开源鸿蒙的编译框架，然后再选一款ARM芯片从第一行代码讲解。

### 1、编译工具

* 编译源码可以使用hb命令(ohos-build)，可以使用build.sh脚本，也可以使用build.py脚本，其中编译脚本在build这个git仓库里，hb命令的ohos-build源码也在build/lite目录下，可以通过这个仓库学习编译整套源码的流程。
  * 对应的仓库地址为：https://gitee.com/openharmony/build
  * 使用了Gn、Ninja、Python工具，类似于Linux中的Cmake、Makefile、Kconfig、Kbuild这些
* Ninja是谷歌用于替代Makefile的开源编译工具，使用并行机制提高速度，源码地址是 https://gitee.com/openharmony/third_party_ninja
  * [Ninja 构建系统-介绍](https://blog.csdn.net/fuhanghang/article/details/113968735)

#### 1.1 Ninja软件学习

* 一个能直接运行的Ninja例子，build.ninja文件怎么写？
  * [CSDN文章阅读地址](https://blog.csdn.net/qq582880551/article/details/136564417)
  * [Gitee源码和工程下载地址](https://gitee.com/langcai1943/OH-NEXT-Kernel)

* 如果你使用的是Windows + MSYS2 + MinGW64
  * 已提前安装过gcc
  * msys2命令行中使用 pacman -S mingw-w64-x86_64-ninja 安装Ninja软件
    * 如何搜索msys2中的软件 https://packages.msys2.org/package/mingw-w64-x86_64-ninja?repo=mingw64

```shell
jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel
$ pacman -S mingw-w64-x86_64-ninja
resolving dependencies...
looking for conflicting packages...

Packages (1) mingw-w64-x86_64-ninja-1.11.1-2

Total Download Size:   0.44 MiB
Total Installed Size:  1.73 MiB

:: Proceed with installation? [Y/n] y
:: Retrieving packages...
 mingw-w64-x86_64-ninja-1.1...   449.2 KiB   286 KiB/s 00:02 [###############################] 100%
(1/1) checking keys in keyring                               [###############################] 100%
(1/1) checking package integrity                             [###############################] 100%
(1/1) loading package files                                  [###############################] 100%
(1/1) checking for file conflicts                            [###############################] 100%
(1/1) checking available disk space                          [###############################] 100%
:: Processing package changes...
(1/1) installing mingw-w64-x86_64-ninja                      [###############################] 100%

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel
$ ninja --version
1.11.1

```
* 如果是Ubuntu系统，则用下面命令安装软件：
  * sudo apt install ninja-build

* 安装好的软件在msys2的 /mingw64/bin/ 目录下
* 将这个目录加入到Linux也就是msys2的环境变量中，如果你曾经添加过，则就不要重复添加了
  * ```echo export PATH=$PATH:/mingw64/bin >> ~/.bashrc```	打开家目录下的.bashrc隐藏文件，在最后面加入一行：export PATH=$PATH:/mingw64/bin，保存并关闭文件
  * source ~/.bashrc	立即生效环境变量

* 默认的构建文件为当前目录下的 build.ninja 文件，类似于Linux中的Makefile文件
  * ninja软件只实现最基本的功能，一般需要其它上层软件来生成一个build.ninja文件，例如Gn软件
  * 你也可以手写build.ninja文件，就像你手写Makefile文件一样，但是不建议你这么做，所以你也不用看懂ninja配置文件怎么写，以后还是用Gn工具来生成它，下面我会手写一个简单的配置文件，其语法也和Makefile类似，使用了两个空格代替Makefile的tag键。

* 进入到本仓库的ninja演示目录
  * cd /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja

* 新建一个mian.c文件，写上hello world输出

```c
jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ cat main.c
#include <stdio.h>

int main()
{
        printf("hello world\n");
        fflush(stdout);

        while (1);

        return 0;
}
```

* touch build.ninja	新建一个ninja配置文件，写上编译参数

```shell
jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ cat build.ninja
cc = gcc
cflags = -Wall

rule cc
  command = $cc $cflags -c $in -o $out

build main.o: cc main.c

rule link
  command = $cc $cflags $in -o $out

build demo.exe: link main.o

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
```

* 编译命令为 ninja 类似于make的用法
* 清除上次编译结果的命令为 ninja -t clean

```shell
jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ls
build.ninja  main.c

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ninja
[2/2] gcc -Wall main.o -o demo.exe

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ls
build.ninja  demo.exe  main.c  main.o

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ./demo.exe
hello world


jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ninja -t clean
Cleaning... 2 files.

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja
$ ls
build.ninja  main.c

jim@DESKTOP-SVP3BEM MSYS /d/1_git/OH-NEXT-Kernel/src/standalone/01_ninja

```

* 参考网址：
  * [ninja介绍及使用](https://blog.csdn.net/MAX__YF/article/details/125856258)
  * [ninja使用](https://blog.csdn.net/xyyangkun/article/details/103965554)
  * [Ninja构建系统入门--手敲一遍BUILD.ninja](https://blog.csdn.net/qq_32566003/article/details/120502719)
  * [v61.03 鸿蒙内核源码分析(忍者ninja) ](https://zhuanlan.zhihu.com/p/424402353)
  * [Ninja构建系统入门](https://www.cnblogs.com/sandeepin/p/ninja.html)

#### 1.2 Gn软件学习

* Gn软件是用来生成Ninja规则的，类似于Linux中的Automake、Cmake、Qmake工具；
  * Gn软件的源码地址：https://gitee.com/openharmony/third_party_gn
  * Gn在大型工程中好用，在小型工程中太繁琐

* 如果你使用的是Windows + MSYS2 + MinGW64，不能安装Gn，只能使用Linux发行版，如Ubuntu。

* Ubuntu中安装Gn
  * 不能通过apt-get install的方式安装，如果你能架梯子，可以去这个网站下载：https://gn.googlesource.com/gn/  安装包
  * 已安装Git、GCC、Ninja、Python 3、Clang-8或以上版本

* sudo apt-get install clang-8	安装Clang-8或以上版本编译器
* cd /usr/bin/	进入默认软件目录
* sudo ln -s ../lib/llvm-8/bin/clang clang	创建链接
* sudo ln -s ../lib/llvm-8/bin/clang++ clang++	创建链接
* clang --version	测试编译器

```shell
jim@ubuntu:/usr/bin$ clang --version
clang version 8.0.0-3~ubuntu18.04.2 (tags/RELEASE_800/final)
Target: x86_64-pc-linux-gnu
Thread model: posix
InstalledDir: /usr/bin
```

* 在命令行中进入你想要的目录，如~/openHarmony
* git clone git@gitee.com:openharmony/third_party_gn.git	下载Gn软件源码
  * 注意：如果用开源鸿蒙的Gn源码仓库，亲测./out/gn_unittests自测时会报错，但不影响使用，使用官方库就不会报错
  * [【OpenHarmony】环境安装及代码下拉开发编译操作总结](https://blog.csdn.net/weixin_60917883/article/details/134084026)
* cd third_party_gn/	进入源码目录
* python build/gen.py	生成Ninja编译文件，类似于Makefile文件
* ninja -C out	编译，时间有点久，要几分钟

```shell
jim@ubuntu:~/0_Git/third_party_gn$ ninja -C out
ninja: Entering directory `out'
[297/297] LINK gn_unittests
```

* ./out/gn_unittests	测试Gn软件，如果你是使用Gn官网的源码会测试正常，如果用开源鸿蒙的源码，则会出现下面报错，忽略下面报错，直接下一步

```shell
jim@ubuntu:~/0_Git/third_party_gn$ ./out/gn_unittests
[390/687] NinjaRustBinaryTargetWriterTest.TransitiveRustDeps[139769390700416:0311/003324.720801:FATAL:values.cc(239)] Check failed: is_list(). 

Aborted (core dumped)
```
* sudo cp ./out/gn /usr/bin	将编译完的程序拷贝到默认程序路径，不这么做的话就需要你自己加环境变量了，make install命令也是类似的作用

* 使用gn help查看是否安装成功

```shell
jim@ubuntu:~/0_Git/third_party_gn$ gn help

Commands (type "gn help <command>" for more help):
  analyze: Analyze which targets are affected by a list of files.
  args: Display or configure arguments declared by the build.
  check: Check header dependencies.
  clean: Cleans the output directory.
  clean_stale: Cleans the stale output files from the output directory.
  desc: Show lots of insightful information about a target or config.
  format: Format .gn files.
  gen: Generate ninja files.
  help: Does what you think.
  ls: List matching targets.
  meta: List target metadata collection results.
  outputs: Which files a source/target make.
  path: Find paths between two targets.
  refs: Find stuff referencing a target or file.

```

* 参考网址：
  * [GN环境搭建](https://blog.csdn.net/weixin_43288065/article/details/128037209)
  * [【嵌入式开发基础】gn ninja命令安装](https://blog.csdn.net/love131452098/article/details/120000265)
  * [gn入门(Chromium)](https://blog.csdn.net/arv002/article/details/135244976)
  * [GN使用笔记](https://blog.csdn.net/juruiyuan111/article/details/115488493)
  * [跨平台：GN实践详解](https://blog.csdn.net/weixin_44701535/article/details/88355958)
  * [MobaXterm安装ninja和GN环境搭建](https://blog.csdn.net/weixin_60917883/article/details/133990450)

#### 1.3 写BUILD.gn文件

* Gn + Ninja编译一个Hello world程序的例子Demo
* Ninja安装流程见：[一个能直接运行的Ninja例子，build.ninja文件怎么写？](https://blog.csdn.net/qq582880551/article/details/136564417)
* Gn安装流程见：[Ubuntu18.04下安装Gn软件](https://blog.csdn.net/qq582880551/article/details/136627327)

* 这是一个简单的BUILD.gn配置文件

```shell
jim@ubuntu:~/0_Git/third_party_gn/examples/simple_build$ cat BUILD.gn 
# Copyright 2014 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

executable("hello") {
  sources = [ "hello.cc" ]

  deps = [
    ":hello_shared",
    ":hello_static",
  ]
}

shared_library("hello_shared") {
  sources = [
    "hello_shared.cc",
    "hello_shared.h",
  ]

  defines = [ "HELLO_SHARED_IMPLEMENTATION" ]
}

static_library("hello_static") {
  sources = [
    "hello_static.cc",
    "hello_static.h",
  ]
}
```

* 这是Gn官方的例子，在Gn的源码里面
  * 使用Ubuntu系统，安装Git、Ninja、Gn、Clang软件
  * git clone git@gitee.com:openharmony/third_party_gn.git    拉取源码，或者：
  * https://gitee.com/openharmony/third_party_gn/repository/archive/master.zip    下载源码

* cd examples/simple_build/    下载源码后进入其中的子目录
* ls    先看看里面的文件

```shell
jim@ubuntu:~/0_Git/third_party_gn/examples/simple_build$ ls
build  BUILD.gn  hello.cc  hello_shared.cc  hello_shared.h  hello_static.cc  hello_static.h  README.md  tutorial

jim@ubuntu:~/0_Git/third_party_gn/examples/simple_build$ cat hello.cc
// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <stdio.h>

#include "hello_shared.h"
#include "hello_static.h"

int main(int argc, char* argv[]) {
  printf("%s, %s\n", GetStaticText(), GetSharedText());
  return 0;
}
```

* gn gen -C out    按Gn规则文件生成Ninja配置文件

```shell
jim@ubuntu:~/0_Git/third_party_gn/examples/simple_build$ gn gen -C out
Done. Made 3 targets from 4 files in 45ms
```

* cd out/    进入输出目录，看看生成的什么内容

```shell
jim@ubuntu:~/0_Git/third_party_gn/examples/simple_build$ tree out/
out/
├── args.gn
├── build.ninja
├── build.ninja.d
├── obj
│   ├── hello.ninja
│   ├── hello_shared.ninja
│   └── hello_static.ninja
└── toolchain.ninja

1 directory, 7 files


jim@ubuntu:~/0_Git/third_party_gn/examples/simple_build$ cat out/build.ninja
ninja_required_version = 1.7.2

rule gn
  command = ../../../../../../../usr/bin/gn --root=./.. -q --C --regeneration gen .
  pool = console
  description = Regenerating ninja files

build build.ninja: gn
  generator = 1
  depfile = build.ninja.d

subninja toolchain.ninja

build hello_shared: phony ./libhello_shared.so
build hello_static: phony obj/libhello_static.a
build $:hello: phony hello
build $:hello_shared: phony ./libhello_shared.so
build $:hello_static: phony obj/libhello_static.a

build all: phony $
    hello $
    ./libhello_shared.so $
    obj/libhello_static.a

default all
```

* ninja    开始编译（和make命令类似）

```shell
jim@ubuntu:~/0_Git/third_party_gn/examples/simple_build/out$ ninja
[6/6] LINK hello

jim@ubuntu:~/0_Git/third_party_gn/examples/simple_build/out$ tree
.
├── args.gn
├── build.ninja
├── build.ninja.d
├── hello
├── libhello_shared.so
├── obj
│   ├── hello.hello.o
│   ├── hello.ninja
│   ├── hello_shared.ninja
│   ├── hello_static.ninja
│   ├── libhello_shared.hello_shared.o
│   ├── libhello_static.a
│   └── libhello_static.hello_static.o
└── toolchain.ninja

1 directory, 13 files
```

* ./hello   运行编译后的可执行文件

```shell
jim@ubuntu:~/0_Git/third_party_gn/examples/simple_build/out$ ./hello 
Hello, world
```

* 参考网址：
  * [GN环境搭建](https://blog.csdn.net/weixin_43288065/article/details/128037209) 

### 2、build编译和配置模块学习

* 开源鸿蒙build编译和配置模块的源码和文档学习
* [CSDN阅读地址](https://blog.csdn.net/qq582880551/article/details/136643256)

#### 2.1 编译模块整体描述

* 目的：使用这个模块里的脚本、配置来编译整个开源鸿蒙系统，生成一个操作系统可执行文件
  * build模块源码地址：https://gitee.com/openharmony/build
  * 基于Gn和ninja的编译构建框架（类似于CMake/QMake/Automake + Makefile）
  * 使用Python文件、Python插件或者bash脚本安装依赖工具、按要求生成BUILD.gn配置文件
  * [build模块介绍 - 编译构建](https://gitee.com/openharmony/build/blob/master/README_zh.md)
* 编译的方法有三种：
  * 1：运行Python源码，生成BUILD.gn并编译
  * 2：运行使用Python写的插件hb，生成BUILD.gn并编译
  * 3：运行bash脚本，生成BUILD.gn并编译

##### 第一种编译，使用Python源码：
  * python3 build.py -p qemu_mini_system_demo@ohemu    这是编译命令
  * [Docker编译环境](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/get-code/gettools-acquire.md/)
  * build.py文件在OpenHarmony源码文件夹的顶层，但其实顶层的文件只是一个链接：build.py -> build/build_scripts/build.py
  * 这是build.py文件的内容：

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import importlib
import subprocess

def search(findir: str, target: str) -> str or bool:
    for root, _, files in os.walk(findir):
        if target in files:
            return root
    return False

def find_top() -> str:
    cur_dir = os.getcwd()
    while cur_dir != "/":
        build_config_file = os.path.join(
            cur_dir, 'build/config/BUILDCONFIG.gn')
        if os.path.exists(build_config_file):
            return cur_dir
        cur_dir = os.path.dirname(cur_dir)

def get_python(python_relative_dir: str) -> str:
    topdir = find_top()
    if python_relative_dir is None:
        python_relative_dir = 'prebuilts/python'
    python_base_dir = os.path.join(topdir, python_relative_dir)

    if os.path.exists(python_base_dir):
        python_dir = search(python_base_dir, 'python3')
        return os.path.join(python_dir, 'python3')
    else:
        print("please execute build/prebuilts_download.sh.",
            "if you used '--python-dir', check whether the input path is valid.")
        sys.exit()

def check_output(cmd: str, **kwargs) -> str:
    process = subprocess.Popen(cmd,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               universal_newlines=True,
                               **kwargs)
    for line in iter(process.stdout.readline, ''):
        sys.stdout.write(line)
        sys.stdout.flush()

    process.wait()
    ret_code = process.returncode

    return ret_code

def build(path: str, args_list: list) -> str:
    python_dir = None
    if "--python-dir" in args_list:
        index = args_list.index("--python-dir")
        if index < len(args_list) - 1:
            python_dir = args_list[index + 1]
            del args_list[index: index + 2]
        else:
            print("-python-dir parmeter missing value.")
            sys.exit()
    python_executable = get_python(python_dir)
    cmd = [python_executable, 'build/hb/main.py', 'build'] + args_list
    return check_output(cmd, cwd=path)

def main():
    root_path = find_top()
    return build(root_path, sys.argv[1:])

if __name__ == "__main__":
    sys.exit(main())
```

##### 第二种编译，运行Python插件：
  * [HB构建工具使用指导](https://gitee.com/openharmony/build/blob/master/hb/README_zh.md)
  * 使用Python的插件ohos-build，使用命令为  hb set; hb build -f  安装流程为
    * python3 -m pip install --user ohos-build==0.4.3  或者
    * python3 -m pip install --user build/hb
    * hb源码在OpenHarmony源码的./build/hb文件夹下：

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from setuptools import setup

from resources.global_var import VERSION

LONG_DESCRIPTION = ""
WORK_PATH = os.path.abspath(os.path.dirname('__file__'))
README_PATH = os.path.join(WORK_PATH, 'README.md')
with open(README_PATH, 'r', encoding='utf-8') as FILE_DESCRIPTION:
    LONG_DESCRIPTION = FILE_DESCRIPTION.read()

setup(
    name='ohos-build',
    version=VERSION,
    author='Huawei',
    author_email='contact@openharmony.io',
    description='OHOS build command line tool',
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url='https://gitee.com/openharmony/build_lite',
    license='Apache 2.0',
    python_requires='>=3.8',
    packages=['hb'],
    package_dir={'hb': 'hb'},
    package_data={'hb': ['common/config.json']},
    install_requires=[
        'prompt_toolkit==1.0.14', 'kconfiglib>=14.1.0', 'PyYAML', 'requests'
    ],
    entry_points={'console_scripts': [
        'hb=hb.__main__:main',
    ]},
)
```

##### 第三种编译，使用脚本：

  * ./build.sh --product-name {product_name}
  * build.sh文件在OpenHarmony源码文件夹的顶层，但其实顶层的文件只是一个链接：build.sh -> build/build_scripts/build.sh
  * 这是build.sh文件的内容：

```shell
#!/bin/bash

set -e
set +e
echo -e "\n\033[32m\t*********Welcome to OpenHarmony!*********\033[0m\n"
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
function check_shell_environment() {
  case $(uname -s) in 
    Linux)
          shell_result=$(/bin/sh -c 'echo ${BASH_VERSION}')
          if [ -n "${shell_result}" ]; then
            echo -e "\033[32mSystem shell: bash ${shell_result}\033[0m"
          else
            echo -e "\033[33m Your system shell isn't bash, we recommend you to use bash, because some commands may not be supported in other shells, such as pushd and shopt are not supported in dash. \n You can follow these tips to modify the system shell to bash on Ubuntu: \033[0m"
            echo -e "\033[33m [1]:Open the Terminal tool and execute the following command: sudo dpkg-reconfigure dash \n [2]:Enter the password and select <no>  \033[0m"
          fi
          ;;
    Darwin)
          echo -e "\033[31m[OHOS ERROR] Darwin system is not supported yet\033[0m"
          ;;
    *)
          echo -e "\033[31m[OHOS ERROR] Unsupported this system: $(uname -s)\033[0m"
          exit 1
  esac
}

check_shell_environment 

echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo -e "\033[32mCurrent time: $(date +%F' '%H:%M:%S)\033[0m"
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo -e "\033[32mBuild args: $@\033[0m"
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

export SOURCE_ROOT_DIR=$(cd $(dirname $0);pwd)

while [[ ! -f "${SOURCE_ROOT_DIR}/.gn" ]]; do
    SOURCE_ROOT_DIR="$(dirname "${SOURCE_ROOT_DIR}")"
    if [[ "${SOURCE_ROOT_DIR}" == "/" ]]; then
        echo -e "\033[31m[OHOS ERROR] Cannot find source tree containing $(pwd)\033[0m"
        exit 1
    fi
done

if [[ "${SOURCE_ROOT_DIR}x" == "x" ]]; then
  echo -e "\033[31m[OHOS ERROR] SOURCE_ROOT_DIR cannot be empty.\033[0m"
  exit 1
fi

case $(uname -s) in
    Darwin)
        HOST_DIR="darwin-x86"
        HOST_OS="mac"
        NODE_PLATFORM="darwin-x64"
        ;;
    Linux)
        HOST_DIR="linux-x86"
        HOST_OS="linux"
        NODE_PLATFORM="linux-x64"
        ;;
    *)
        echo "\033[31m[OHOS ERROR] Unsupported host platform: $(uname -s)\033[0m"
        RET=1
        exit $RET
esac

# set python3
PYTHON3_DIR=${SOURCE_ROOT_DIR}/prebuilts/python/${HOST_DIR}/current/
PYTHON3=${PYTHON3_DIR}/bin/python3
PYTHON=${PYTHON3_DIR}/bin/python
if [[ ! -f "${PYTHON3}" ]]; then
  echo -e "\033[31m[OHOS ERROR] Please execute the build/prebuilts_download.sh \033[0m"
  exit 1
else
  if [[ ! -f "${PYTHON}" ]]; then
    ln -sf "${PYTHON3}" "${PYTHON}"
  fi
fi

export PATH=${SOURCE_ROOT_DIR}/prebuilts/build-tools/${HOST_DIR}/bin:${PYTHON3_DIR}/bin:$PATH

# set nodejs and ohpm
EXPECTED_NODE_VERSION="14.21.1"
export PATH=${SOURCE_ROOT_DIR}/prebuilts/build-tools/common/nodejs/node-v${EXPECTED_NODE_VERSION}-${NODE_PLATFORM}/bin:$PATH
export NODE_HOME=${SOURCE_ROOT_DIR}/prebuilts/build-tools/common/nodejs/node-v${EXPECTED_NODE_VERSION}-${NODE_PLATFORM}
export PATH=${SOURCE_ROOT_DIR}/prebuilts/build-tools/common/oh-command-line-tools/ohpm/bin:$PATH
echo "[OHOS INFO] Current Node.js version is $(node -v)"
NODE_VERSION=$(node -v)
if [ "$NODE_VERSION" != "v$EXPECTED_NODE_VERSION" ]; then
  echo -e "\033[31m[OHOS ERROR] Node.js version mismatch. Expected $EXPECTED_NODE_VERSION but found $NODE_VERSION\033[0m" >&2
  exit 1
fi
echo -e "\033[32m[OHOS INFO] Node.js version check passed!\033[0m"
npm config set registry https://repo.huaweicloud.com/repository/npm/
npm config set @ohos:registry https://repo.harmonyos.com/npm/
npm config set strict-ssl false
npm config set lockfile false
cat $HOME/.npmrc | grep 'lockfile=false' > /dev/null || echo 'lockfile=false' >> $HOME/.npmrc > /dev/null

function init_ohpm() {
  TOOLS_INSTALL_DIR="${SOURCE_ROOT_DIR}/prebuilts/build-tools/common"
  pushd ${TOOLS_INSTALL_DIR} > /dev/null
    if [[ ! -f "${TOOLS_INSTALL_DIR}/oh-command-line-tools/ohpm/bin/ohpm" ]]; then
      echo "[OHOS INFO] download oh-command-line-tools"
      wget https://contentcenter-vali-drcn.dbankcdn.cn/pvt_2/DeveloperAlliance_package_901_9/68/v3/r-5H8I7LT9mBjSFpSOY0Sg/ohcommandline-tools-linux-2.1.0.6.zip\?HW-CC-KV\=V1\&HW-CC-Date\=20231027T004601Z\&HW-CC-Expire\=315360000\&HW-CC-Sign\=A4D5E1A29C1C6962CA65592C3EB03ED363CE664CBE6C5974094064B67C34325E -O ohcommandline-tools-linux.zip
      unzip ohcommandline-tools-linux.zip
    fi
    OHPM_HOME=${TOOLS_INSTALL_DIR}/oh-command-line-tools/ohpm
    chmod +x ${OHPM_HOME}/bin/init
    ${OHPM_HOME}/bin/init > /dev/null
    echo "[OHOS INFO] Current ohpm version is $(ohpm -v)"
    ohpm config set registry https://repo.harmonyos.com/ohpm/
    ohpm config set strict_ssl false
    ohpm config set log_level debug
  popd > /dev/null
  if [[ -d "$HOME/.hvigor" ]]; then
    rm -rf $HOME/.hvigor/daemon $HOME/.hvigor/wrapper
  fi
  mkdir -p $HOME/.hvigor/wrapper/tools
  echo '{"dependencies": {"pnpm": "7.30.0"}}' > $HOME/.hvigor/wrapper/tools/package.json
  pushd $HOME/.hvigor/wrapper/tools > /dev/null
    echo "[OHOS INFO] installing pnpm..."
    npm install --silent > /dev/null
  popd > /dev/null
  mkdir -p $HOME/.ohpm
  echo '{"devDependencies":{"@ohos/hypium":"1.0.6"}}' > $HOME/.ohpm/oh-package.json5
  pushd $HOME/.ohpm > /dev/null
    echo "[OHOS INFO] installing hypium..."
    ohpm install > /dev/null
  popd > /dev/null
}

if [[ "$*" != *ohos-sdk* ]]; then
  echo "[OHOS INFO] Ohpm initialization started..."
  init_ohpm
  if [[ "$?" -ne 0 ]]; then
    echo -e "\033[31m[OHOS ERROR] ohpm initialization failed!\033[0m"
    exit 1
  fi
  echo -e "\033[32m[OHOS INFO] ohpm initialization successful!\033[0m"
fi
echo -e "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"

echo -e "\033[32m[OHOS INFO] Start building...\033[0m\n"
function build_sdk() {
    ROOT_PATH=${SOURCE_ROOT_DIR}
    SDK_PREBUILTS_PATH=${ROOT_PATH}/prebuilts/ohos-sdk

    pushd ${ROOT_PATH} > /dev/null
      echo -e "[OHOS INFO] building the latest ohos-sdk..."
      ./build.py --product-name ohos-sdk $ccache_args $xcache_args --load-test-config=false --get-warning-list=false --stat-ccache=false --compute-overlap-rate=false --deps-guard=false --generate-ninja-trace=false --gn-args skip_generate_module_list_file=true sdk_platform=linux ndk_platform=linux use_cfi=false use_thin_lto=false enable_lto_O0=true sdk_check_flag=false enable_ndk_doxygen=false archive_ndk=false sdk_for_hap_build=true
      if [[ "$?" -ne 0 ]]; then
        echo -e "\033[31m[OHOS ERROR] ohos-sdk build failed! You can try to use '--no-prebuilt-sdk' to skip the build of ohos-sdk.\033[0m"
        exit 1
      fi
      if [ -d "${ROOT_PATH}/prebuilts/ohos-sdk/linux" ]; then
          rm -rf ${ROOT_PATH}/prebuilts/ohos-sdk/linux
      fi
      mkdir -p ${SDK_PREBUILTS_PATH}
      mv ${ROOT_PATH}/out/sdk/ohos-sdk/linux ${SDK_PREBUILTS_PATH}/
      mkdir -p ${SDK_PREBUILTS_PATH}/linux/native
      mv ${ROOT_PATH}/out/sdk/sdk-native/os-irrelevant/* ${SDK_PREBUILTS_PATH}/linux/native/
      mv ${ROOT_PATH}/out/sdk/sdk-native/os-specific/linux/* ${SDK_PREBUILTS_PATH}/linux/native/
      pushd ${SDK_PREBUILTS_PATH}/linux > /dev/null
        api_version=$(grep apiVersion toolchains/oh-uni-package.json | awk '{print $2}' | sed -r 's/\",?//g') || api_version="11"
        mkdir -p $api_version
        for i in */; do
            if [ -d "$i" ] && [ "$i" != "$api_version/" ]; then
                mv $i $api_version
            fi
        done
      popd > /dev/null
    popd > /dev/null
}
if [[ ! -d "${SOURCE_ROOT_DIR}/prebuilts/ohos-sdk/linux" && "$*" != *ohos-sdk* && "$*" != *"--no-prebuilt-sdk"* || "${@}" =~ "--prebuilt-sdk" ]]; then
  echo -e "\033[33m[OHOS INFO] The OHOS-SDK was not detected, so the SDK compilation will be prioritized automatically. You can also control whether to execute this process by using '--no-prebuilt-sdk' and '--prebuilt-sdk'.\033[0m"
  if [[ "${@}" =~ "--ccache=false" || "${@}" =~ "--ccache false" ]]; then
    ccache_args="--ccache=false"
  else
    ccache_args="--ccache=true"
  fi
  if [[ "${@}" =~ "--xcache=true" || "${@}" =~ "--xcache true" || "${@}" =~ "--xcache" ]]; then
    xcache_args="--xcache=true"
  else
    xcache_args="--xcache=false"
  fi
  build_sdk
  if [[ "$?" -ne 0 ]]; then
    echo -e "\033[31m[OHOS ERROR] ohos-sdk build failed, please remove the out/sdk directory and try again!\033[0m"
    exit 1
  fi
fi

${PYTHON3} ${SOURCE_ROOT_DIR}/build/scripts/tools_checker.py

flag=true
args_list=$@
for var in $@
do
  OPTIONS=${var%%=*}
  PARAM=${var#*=}
  if [[ "$OPTIONS" == "using_hb_new" && "$PARAM" == "false" ]]; then
    flag=false
    ${PYTHON3} ${SOURCE_ROOT_DIR}/build/scripts/entry.py --source-root-dir ${SOURCE_ROOT_DIR} $args_list
    break
  fi
done
if [[ ${flag} == "true" ]]; then
  ${PYTHON3} ${SOURCE_ROOT_DIR}/build/hb/main.py build $args_list
fi

if [[ "$?" -ne 0 ]]; then
    echo -e "\033[31m=====build ${product_name} error=====\033[0m"
    exit 1
fi
echo -e "\033[32m=====build ${product_name} successful=====\033[0m"

date +%F' '%H:%M:%S
echo "++++++++++++++++++++++++++++++++++++++++"
```

#### 2.2 OpenHarmony编译前的系统配置

* 进行过Linux内核编程的就知道，make menuconfig里面有很多配置，一个操作系统要进行定制，编译静态库动态库，就需要在编译前进行各种配置。
  * [开源鸿蒙配置文件说明](https://gitee.com/openharmony/build/blob/master/README_zh.md#%E8%AF%B4%E6%98%8E)
* OpenHarmony的配置方法：
  * 1：可以使用gn语法规则自定义编写模块内的BUILD.gn文件（有模块模板）
  * 2：可以手动修改bundle.json、ohos.build等文件
  * 3：编译时直接把配置输进去，如：./build.sh --product-name ohos-sdk --ccache
* 使用方法：修改.json和.gn文件
* 鸿蒙build编译子模块中有个顶层的bundle.json配置：

```json
{
  "name": "@ohos/build_framework",
  "description": "build framework",
  "version": "4.0.2",
  "license": "Apache License 2.0",
  "homePage":"https://gitee.com/openharmony",
  "repository":"https://gitee.com/openharmony/build",
  "supplier": "Organization: OpenHarmony",
  "publishAs": "code-segment",
  "segment": {
      "destPath": "build"
  },
 "readmePath":{
      "zh": "README_zh.md"
 },
 "dirs": {},
 "scripts": {
      "install": "cd ${DEP_BUNDLE_BASE} && ln -snf build/build_scripts/build.py build.py && ln -snf build/build_scripts/build.sh build.sh && ln -snf build/core/gn/dotfile.gn .gn"
 },
  "component": {
    "name": "build_framework",
    "description": "build_framework component set",
    "subsystem": "build",
    "features": [],
    "adapted_system_type": [
      "standard",
      "small",
      "mini"
    ],
    "rom": "0KB",
    "ram": "0KB",
    "deps": {
      "components": [],
        "third_party": [
        "musl",
        "markupsafe",
        "jinja2",
        "e2fsprogs",
        "f2fs-tools"
      ]
    },
    "build": {
      "sub_component": [
        "//build/common:common_packages",
        "//build/rust:default",
        "//third_party/f2fs-tools:f2fs-tools_host_toolchain"
      ],
      "inner_api": [
        {
          "name": "//build/rust:libstd.dylib.so",
          "header": {
            "header_base": [],
            "header_files": []
          }
        },
        {
          "name": "//build/rust:libtest.dylib.so",
          "header": {
            "header_files": [],
            "header_base": []
          }
        }
      ],
      "test": [
        "//build/rust/tests:tests"
      ]
    }
  }
}
```

* ./core/gn下有个BUILD.gn文件：

```c
print("root_out_dir=$root_out_dir")
print("root_build_dir=$root_build_dir")
print("root_gen_dir=$root_gen_dir")
print("current_toolchain=$current_toolchain")
print("host_toolchain=$host_toolchain")

import("//build/ohos_var.gni")

# gn target defined
if (product_name == "ohos-sdk") {
  group("build_ohos_sdk") {
    deps = [
      "//build/ohos/ndk:ohos_ndk",
      "//build/ohos/sdk:ohos_sdk",
      "//build/ohos/sdk:ohos_sdk_verify",
    ]
  }
} else if (product_name == "arkui-x") {
  group("arkui_targets") {
    deps = [ "//build_plugins/sdk:arkui_cross_sdk" ]
  }
} else {
  group("make_all") {
    deps = [
      ":make_inner_kits",
      ":packages",
    ]
    if (is_standard_system && !is_llvm_build) {
      # Lite system uses different packaging scheme, which is called in hb.
      # So skip images for lite system since it's the mkimage
      # action for standard system.
      deps += [ ":images" ]
    }
  }

  if (!is_llvm_build) {
    group("images") {
      deps = [ "//build/ohos/images:make_images" ]
    }
  }

  group("packages") {
    deps = [ "//build/ohos/packages:make_packages" ]
  }

  group("make_inner_kits") {
    deps = [ "$root_build_dir/build_configs:inner_kits" ]
  }

  group("build_all_test_pkg") {
    testonly = true
    if (!is_llvm_build) {
      deps = [
        "$root_build_dir/build_configs:parts_test",
        "//test/testfwk/developer_test:make_temp_test",
      ]
    }
  }

  group("make_test") {
    testonly = true
    deps = [
      "//build/ohos/packages:build_all_test_pkg",
      "//build/ohos/packages:package_testcase",
      "//build/ohos/packages:package_testcase_mlf",
    ]
    if (archive_component) {
      deps += [ "//build/ohos/testfwk:archive_testcase" ]
    }
  }
}
```

### 3、boot代码

* 开源鸿蒙OpenHarmony上电的第一行代码，boot代码

* 上电的第一行代码是硬件复位中断的入口，也就是RAM零地址的代码，属于boot代码的一部分
* [芯片启动流程 - 内核概述](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/kernel/kernel-mini-overview.md/)
* 内核代码具体的细节，ARM Cortex-M可以参考LiteOS-M，ARM Cortex-A可以参考LiteOS-A或者Linux内核
  * [LiteOS-M内核介绍](https://gitee.com/openharmony/kernel_liteos_m)
  * [LiteOS-A内核介绍](https://gitee.com/openharmony/kernel_liteos_a)
  * [Linux5.10内核鸿蒙特供版](https://gitee.com/openharmony/kernel_linux_5.10)
  * 因为当前代码中Cortex-M3只支持Keil，Cortex-M4支持GCC

* 本来芯片上电后的第一行代码应该是能在源码中看到的，但是OpenHarmony中使用了ARM的CMSIS官方库，也就是说上电时的第一行代码已经融合进编译器里面去了，所以你看不到，只能看到第二行代码，那就是 Reset_Handler() 复位中断函数
  * RAM的0地址就是第一行代码，同时也是reset复位中断的入口，在这里的代码只有一个目的，那就是复位中断进来后立马调用 Reset_Handler() 函数，这里既可以调用汇编的reset函数，也可以调用C语言函数。
* 搜索 Reset_Handler 这个函数，这是上电后执行的第二行代码
  * ```arm/cortex-m4/gcc/los_interrupt.c:    hwiForm[1] = (HWI_PROC_FUNC)Reset_Handler; /* [1] reset */```
  * ```arm/cortex-m3/keil/los_startup.s:Reset_Handler```

* ARM官方CMSIS的API一般都是NVIC_开头的函数
* 上电后先调用的函数有：
  * HalHwiInit
  * ArchInit
  * LOS_KernelInit
  * kal/cmsis/cmsis_liteos2.c
  * osKernelInitialize

```shell
jim@ubuntu:~/openHarmony/kernel/liteos_m/arch/arm/cortex-m3/keil$ ls
los_arch_atomic.h   los_arch_interrupt.h  los_atomic.S   los_dispatch.S  los_interrupt.c  los_timer.c
los_arch_context.h  los_arch_timer.h      los_context.c  los_exc.S       los_startup.s
```

```c
jim@ubuntu:~/openHarmony$ cat kernel/liteos_m/arch/arm/cortex-m4/gcc/los_interrupt.c 

#include <stdarg.h>
#include "securec.h"
#include "los_context.h"
#include "los_arch_interrupt.h"
#include "los_hook.h"
#include "los_task.h"
#include "los_sched.h"
#include "los_memory.h"
#include "los_membox.h"
#if (LOSCFG_CPUP_INCLUDE_IRQ == 1)
#include "los_cpup.h"
#endif

/* ****************************************************************************
 Function    : HalHwiInit
 Description : initialization of the hardware interrupt
 Input       : None
 Output      : None
 Return      : None
 **************************************************************************** */
LITE_OS_SEC_TEXT_INIT VOID HalHwiInit(VOID)
{
#if (LOSCFG_USE_SYSTEM_DEFINED_INTERRUPT == 1)
    UINT32 index;
    HWI_PROC_FUNC *hwiForm = (HWI_PROC_FUNC *)ArchGetHwiFrom();
    hwiForm[0] = 0; /* [0] Top of Stack */
    hwiForm[1] = (HWI_PROC_FUNC)Reset_Handler; /* [1] reset */
    for (index = 2; index < OS_VECTOR_CNT; index++) { /* 2: The starting position of the interrupt */
        hwiForm[index] = (HWI_PROC_FUNC)HalHwiDefaultHandler;
    }

    /* Interrupt vector table location */
    SCB->VTOR = (UINT32)(UINTPTR)hwiForm;
#endif
#if (__CORTEX_M >= 0x03U) /* only for Cortex-M3 and above */
    NVIC_SetPriorityGrouping(OS_NVIC_AIRCR_PRIGROUP);
#endif

    return;
}

/* ======== */

jim@ubuntu:~/openHarmony/third_party$ cat cmsis/Device/ARM/ARMCM55/Source/startup_ARMCM55.c

/*----------------------------------------------------------------------------
  Reset Handler called on controller reset
 *----------------------------------------------------------------------------*/
__NO_RETURN void Reset_Handler(void)
{
  __set_PSP((uint32_t)(&__INITIAL_SP));

  __set_MSPLIM((uint32_t)(&__STACK_LIMIT));
  __set_PSPLIM((uint32_t)(&__STACK_LIMIT));

#if defined (__ARM_FEATURE_CMSE) && (__ARM_FEATURE_CMSE == 3U)
  __TZ_set_STACKSEAL_S((uint32_t *)(&__STACK_SEAL));
#endif

  SystemInit();                             /* CMSIS System Initialization */
  __PROGRAM_START();                        /* Enter PreMain (C library entry point) */
}
```

* 这个复位中断里会一级一级往后调用，你建个工程在代码里面一步步跳转就可以溯源了。

### 4、芯片与开发板对应的源码

* 开源鸿蒙通过芯片仓存放指定芯片和指定开发板的代码，硬件相关的代码和纯逻辑代码是分开存放的
  * 源码模块的组织结构在manifest这个Git仓库，这也是拉取源码时的顶层仓库：```https://gitee.com/openharmony/manifest```
  * 平台仓都组织在```manifests/ohos/ohos.xml```文件中，而芯片仓都组织在```manifests/chipsets/``` 目录下
  * 每个芯片平台会在device和vendor目录下创建相应的仓，把这类仓称为芯片仓，其它的仓称为平台仓，芯片仓可能会随着硬件的演进而逐渐废弃，生命周期相对较短
  * default.xml由ohos/ohos.xml和chipsets/all.xml组成，是所有平台仓和芯片仓的集合；可以通过缺省参数下载所有代码仓（全量代码）
  * chipsets/chipsetN/chipsetN-detail.xml是单个芯片平台所引入的仓集合
  * 每个开发板的chipsets/chipsetN/chipsetN-detail.xml里主要包括device/soc，device/board以及vendor相关仓
  * [官方支持的开发板和模拟器种类-编译形态整体说明](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/quick-start/quickstart-appendix-compiledform.md/)

* 因为硬件各种各样，为了学习方便，这里选择几个ARM核的QEMU模拟器（不使用硬件，使用虚拟开发板）
  * 编译参数（产品名）：qemu_arm_linux_headless，开发板名称：qemu-arm-linux，芯片名称：qemu，芯片内核：ARM Cortex-A，系统类型：标准，系统内核：linux，开发板参数：```https://gitee.com/openharmony/vendor_ohemu/blob/master/qemu_arm_linux_headless/config.json```
  * 编译参数（产品名）：qemu_small_system_demo，开发板名称：arm_virt，芯片名称：qemu，芯片内核：ARM Cortex-A，系统类型：小型，系统内核：liteos_a，开发板参数：```https://gitee.com/openharmony/vendor_ohemu/blob/master/qemu_small_system_demo/config.json```
  * 编译参数（产品名）：qemu_mini_system_demo，开发板名称：arm_mps2_an386，芯片名称：qemu，芯片内核：ARM Cortex-M4，系统类型：轻型，系统内核：liteos_m，开发板参数：```https://gitee.com/openharmony/vendor_ohemu/blob/master/qemu_mini_system_demo/config.json```

* verdor芯片仓的开发板配置
  * ```https://gitee.com/openharmony/vendor_ohemu/tree/master/qemu_arm_linux_headless```
  * ```https://gitee.com/openharmony/vendor_ohemu/tree/master/qemu_small_system_demo```
  * ```https://gitee.com/openharmony/vendor_ohemu/tree/master/qemu_mini_system_demo```

* device芯片仓的源码和配置
  * ```https://gitee.com/openharmony/device_qemu/tree/master/arm_virt/linux```
  * ```https://gitee.com/openharmony/device_qemu/tree/master/arm_virt/liteos_a```
  * ```https://gitee.com/openharmony/device_qemu/tree/master/arm_virt/liteos_a_mini```
  * ```https://gitee.com/openharmony/device_qemu/tree/master/arm_mps2_an386```    含上电后的初始化代码
  * ```https://gitee.com/openharmony/device_qemu/tree/master/drivers```

* 参考网址：
  * [QEMU Arm MPS2 and MPS3 boards](https://qemu-stsquad.readthedocs.io/en/docs-review/system/arm/mps2.html)

### 5、源码框架

* 以下配置文件指示了你要下载哪些Git仓库，并将他们拉取到哪个子文件夹下，拉取完成后才会构成完整的开源鸿蒙源码
  * https://gitee.com/openharmony/manifest/blob/master/default.xml  分为平台仓和芯片仓
  * https://gitee.com/openharmony/manifest/blob/master/ohos/ohos.xml  所有与硬件无关的平台仓，按group类型拉取代码，是你的group则拉取，不是则忽略
    * 例如，将 https://gitee.com/openharmony/kernel_liteos_m 仓库拉取到 kernel/liteos_m 目录下
  * https://gitee.com/openharmony/manifest/blob/master/chipsets/all.xml  所有与硬件相关的芯片仓，按芯片名称分类
  * https://gitee.com/openharmony/manifest/blob/master/chipsets/qemu.xml  选中一款芯片，例如选中qemu这款虚拟芯片，平台仓还是选中全量，芯片仓则选中特有的
  * https://gitee.com/openharmony/manifest/blob/master/chipsets/qemu/qemu.xml  qemu这款虚拟芯片的芯片仓代码
    * 将 https://gitee.com/openharmony/vendor_ohemu 这个仓库拉取到 vendor/ohemu 文件夹下，含模块依赖关系、操作系统的配置参数、OEM-ID-密钥源码
    * 将 https://gitee.com/openharmony/device_qemu 这个仓库拉取到 device/qemu 文件夹下，包含芯片相关驱动底层硬件相关部分


* 以ARM Cortex-M内核的arm_mps2_an386开发板为例，简述源码结构
* openHarmony\device\qemu\arm_mps2_an386\liteos_m\board\startup.s（源码网址```https://gitee.com/openharmony/device_qemu/blob/master/arm_mps2_an386/liteos_m/board/startup.s```） 中描述了芯片上电后的第二行代码：复位中断（第一行代码RAM 0地址的复位中断融合到编译器里面去了），在复位中断中跳转到C语言的main函数

```asm
.global Reset_Handler

.section .text
.type  Reset_Handler, %function
Reset_Handler:
    ldr  r0, =__bss_start
    ldr  r1, =__bss_end
    mov  r2, #0

bss_loop:
    str  r2, [r0, #0]
    add  r0, r0, #4
    subs r3, r1, r0
    bne  bss_loop

    ldr  sp, =__irq_stack_top
    b    main
.size  Reset_Handler, .-Reset_Handler
```

* openHarmony\device\qemu\arm_mps2_an386\liteos_m\board\main.c（源码网址```https://gitee.com/openharmony/device_qemu/blob/master/arm_mps2_an386/liteos_m/board/main.c```） 中在main函数里面启动操作系统

```c
/*****************************************************************************
 Function    : main
 Description : Main function entry
 Input       : None
 Output      : None
 Return      : None
 *****************************************************************************/
LITE_OS_SEC_TEXT_INIT int main(void)
{
    unsigned int ret;

    UartInit();

    ret = LOS_KernelInit();
    if (ret != LOS_OK) {
        printf("LiteOS kernel init failed! ERROR: 0x%x\n", ret);
        goto EXIT;
    }
#if (LOSCFG_SUPPORT_LITTLEFS == 1)
    LfsLowLevelInit();
#endif

    Uart0RxIrqRegister();

    NetInit();

#if (LOSCFG_USE_SHELL == 1)
    ret = LosShellInit();
    if (ret != LOS_OK) {
        printf("LosAppInit failed! ERROR: 0x%x\n", ret);
    }
#endif

    ret = LosAppInit();
    if (ret != LOS_OK) {
        printf("LosAppInit failed! ERROR: 0x%x\n", ret);
    }

    LOS_Start();

EXIT:
    while (1) {
        __asm volatile("wfi");
    }
}
```

* openHarmony\device\qemu\arm_mps2_an386\liteos_m\board\driver 包含了芯片的底层驱动代码
* 接下来就是理解操作系统内核模块，当前是LiteOS-M、LiteOS-A和Linux，之后会全部换成鸿蒙内核

---

* 内核框架

* 小型系统使用是LiteOS-M内核，轻量系统使用的是LiteOS-A内核，源自于华为以前的LiteOS物联网系统，但自从鸿蒙系统发展起来后，从2022年开始LiteOS系统就再也没更新过了，所以要学LiteOS还是要在鸿蒙仓库里学（以后鸿蒙内核出来后再去转到该内核）。

## 四、学习借鉴源码中的思想

* OpenHarmony LiteOS-M内核提供了两套操作系统API接口：CMSIS和POSIX
* 软件接口协议
* CMSIS：ARM微控制器软件接口标准
  * 为了使不同厂商的单片机在软件上是兼容的
  * 各芯片原厂按这个标准去写模块驱动，里面比较出名的就是STM32的标准库、HAL库、LL库
  * CMSIS标准还和CPU内核相关，如M0、M3、M4的都不同
  * ARM核内相关的东西以及核内外设的代码由ARM公司单独实现，包括寄存器、Boot、中断等，CPAL是核内外设
  * 片上外设驱动代码由芯片厂商实现，
  * 一些规范的内容其实和某一家公司的C语言编码规范类似，规定了重入、大小写、命名
  * CMSIS也分为了不同的模块，在Keil里可以按需勾选

* POSIX：可移植操作系统接口，使不同操作系统之间的软件兼容
  * 包括文件管理、进程控制、用户权限、系统调用、shell、正则表达式、网络编程等
  * 符合该标准的操作系统：VxWorks、Linux、macOS、FreeBSD、Unix系列等，也就是说除了Windows，基本上其它的大型操作系统都支持POSIX

* [CMSIS到底是个什么东西](https://blog.csdn.net/m0_37845735/article/details/105829019)
* [CMSIS标准](https://blog.csdn.net/liudongdong19/article/details/81534912)
* [Cortex-M微控制器软件接口标准CMSIS详细内容](https://blog.csdn.net/ybhuangfugui/article/details/104871693)
* [POSIX 介绍](https://blog.csdn.net/usstmiracle/article/details/123255930)

* 之前单独的LiteOS是通过Makefile编译的，当前的开源鸿蒙LiteOS-M和LiteOS-A是通过gn和ninja编译的。

* Gitee官方只介绍了LiteOS-M的gn + ninja编译的流程，针对M3使用Keil编译的流程可能要参考社区
  * [harmonyos_next / stm32f103_simulator_keil](https://gitee.com/harmonyos_next/stm32f103_simulator_keil)
  * [OpenHarmony / kernel_liteos_m](https://gitee.com/openharmony/kernel_liteos_m)
  * 使用Keil编译ARM Cortex-M3时，只需要kernel_liteos_m仓库和Keil工程仓库，不需要全量的OpenHarmony代码

* LiteOS-M 编码规范
 * [OpenHarmony轻内核编码规范](https://gitee.com/openharmony/kernel_liteos_m/wikis/OpenHarmony%E8%BD%BB%E5%86%85%E6%A0%B8%E7%BC%96%E7%A0%81%E8%A7%84%E8%8C%83)
 * [OpenHarmony C语言编程规范](https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/OpenHarmony-c-coding-style-guide.md)

* [开发板移植流程](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/porting/Readme-CN.md)
* [轻量系统STM32F407芯片移植案例](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/porting/porting-stm32f407-on-minisystem-eth.md)
  * 确定主频、选定默认打印输出的串口、printf标准输入输出重定向到默认串口、指定内存范围给内核、提供定时器给内核
* [移植芯片内核架构的流程](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/porting/porting-minichip-kernel.md)

* [LiteOS-M内核BUILD.gn编写指南](https://gitee.com/caoruihong/kernel_liteos_m/wikis/LiteOS-M%E5%86%85%E6%A0%B8BUILD.gn%E7%BC%96%E5%86%99%E6%8C%87%E5%8D%97)

### LiteOS-M内核中的代码

* 源码下载：[OpenHarmony / kernel_liteos_m](https://gitee.com/openharmony/kernel_liteos_m)
  * 代码总量只有8M，代码量和uC/OS这类操作系统类似，因为代码量不多，因此一个人也是能分析完的；uC/OS虽然是开源、个人免费，但是商用是要收费的，所以ARM Cortex-M之类的芯片使用LiteOS-M或者RT-Thread这类操作系统还是蛮划算的，需要技术支持的话也可以找相关公司进行付费咨询。
  * 代码结构如下：

```C
jim@DESKTOP-SVP3BEM MINGW64 /d/1_git/openHarmony/kernel/liteos_m (master)
$ tree .
.
|-- BUILD.gn	/* 老版本的LiteOS使用Makefile编译，新版本还可以用gn + ninja */
|-- Kconfig
|-- Makefile
|-- arch		/* 硬件相关的代码，按内核IP公司 - IP系列进行区分 */
|   |-- BUILD.gn
|   |-- Kconfig
|   |-- arm
|   |   |-- BUILD.gn
|   |   |-- Kconfig
|   |   |-- arm9
|   |   |   .......
|   |   |-- common	/* 相同芯片内核IP公司通用的函数和接口 */
|   |   |   |-- BUILD.gn
|   |   |   |-- los_common_interrupt.c
|   |   |   `-- los_common_interrupt.h
|   |   |-- cortex-m3	/* M3只支持Keil编译，之后的版本如M4、Cortex-A则是在Linux + gcc下编译 */
|   |   |   `-- keil	/* 操作系统底层相关的支持，也是操作系统移植的重点 */
|   |   |       |-- los_arch_atomic.h
|   |   |       |-- los_arch_context.h
|   |   |       |-- los_arch_interrupt.h
|   |   |       |-- los_arch_timer.h
|   |   |       |-- los_atomic.S
|   |   |       |-- los_context.c
|   |   |       |-- los_dispatch.S
|   |   |       |-- los_exc.S
|   |   |       |-- los_interrupt.c
|   |   |       |-- los_startup.s	/* 芯片上电后执行的第二行代码，芯片上电后的第一行代码一般都融合进了编译器中，由芯片原厂实现，写代码时看不到 */
|   |   |       `-- los_timer.c		/* 需要给操作系统的定时器接口，用于时间片轮转 */
|   |   |-- cortex-m33
|   |   |   ......
|   |   |-- cortex-m4
|   |   |   .......
|   |   |-- cortex-m55
|   |   |   .......
|   |   |-- cortex-m7
|   |   |   .......
|   |-- csky
|   |   .......
|   |-- include		/* 所有芯片对操作系统暴露出的统一接口 */
|   |   |-- los_arch.h
|   |   |-- los_atomic.h
|   |   |-- los_context.h
|   |   |-- los_interrupt.h
|   |   |-- los_mpu.h
|   |   `-- los_timer.h
|   |-- risc-v
|   |   ......
|   `-- xtensa
|       ......
|	/* arch结构和Linux源码类似，一组类似的模块会暴露出统一的接口放在.h中，而.c的实现会分散到各处的不同模块，这和普通裸机代码中.c和.h在一起的结构不一样 */
|-- bundle.json
|-- components	/* 支持的插件，可选 */
|   |-- BUILD.gn
|   |-- backtrace
|   |   |-- BUILD.gn
|   |   |-- los_backtrace.c
|   |   `-- los_backtrace.h
|   |-- cppsupport	/* 是否支持C++ */
|   |   |-- BUILD.gn
|   |   |-- los_cppsupport.c
|   |   `-- los_cppsupport.h
|   |-- cpup
|   |   |-- BUILD.gn
|   |   |-- los_cpup.c
|   |   `-- los_cpup.h
|   |-- debugtools
|   |   |-- BUILD.gn
|   |   |-- los_debugtools.h
|   |   |-- los_hwidump.c
|   |   |-- los_schedtrace.c
|   |   `-- los_stackdump.c
|   |-- dynlink
|   |   |-- BUILD.gn
|   |   |-- los_dynlink.c
|   |   |-- los_dynlink.h
|   |   `-- script
|   |       `-- so_parse
|   |-- exchook
|   |   |-- BUILD.gn
|   |   |-- los_exc_info.c
|   |   |-- los_exc_info.h
|   |   |-- los_exchook.c
|   |   `-- los_exchook.h
|   |-- fs	/* 操作系统的四大模块之一：文件系统 */
|   |   |-- BUILD.gn
|   |   |-- Kconfig
|   |   |-- fatfs	/* FAT32最常用，可以用于U盘、Flash、网盘等 */
|   |   |   |-- BUILD.gn
|   |   |   |-- Kconfig
|   |   |   |-- fatfs.c
|   |   |   |-- fatfs.h
|   |   |   `-- fatfs_conf.h
|   |   |-- littlefs
|   |   |   ......
|   |   `-- vfs
|   |       ......
|   |-- iar_tls
|   |   |-- los_iar_tls.c
|   |   `-- los_iar_tls.h
|   |-- lmk
|   |   |-- BUILD.gn
|   |   |-- los_lmk.c
|   |   `-- los_lmk.h
|   |-- lms
|   |   |-- BUILD.gn
|   |   |-- Kconfig
|   |   |-- lms_libc.c
|   |   |-- los_lms.c
|   |   |-- los_lms.h
|   |   `-- los_lms_pri.h
|   |-- net		/* 操作系统的第五大功能：通信/网络通信 */
|   |   |-- BUILD.gn
|   |   |-- lwip-2.1	/* 像网络、U盘之类的大型通信类驱动代码会很多，有时甚至会比操作系统本身的代码量还多 */
|   |   |   |-- BUILD.gn
|   |   |   |-- enhancement
|   |   |   |   `-- src
|   |   |   |       |-- fixme.c
|   |   |   |       `-- lwip_ifaddrs.c
|   |   |   |-- lwip_porting.gni
|   |   |   `-- porting
|   |   |       |-- include
|   |   |       |   |-- arch
|   |   |       |   |   |-- cc.h
|   |   |       |   |   |-- perf.h
|   |   |       |   |   `-- sys_arch.h
|   |   |       |   |-- lwip
|   |   |       |   |   |-- api_shell.h
|   |   |       |   |   |-- dhcp.h
|   |   |       |   |   |-- inet.h
|   |   |       |   |   |-- lwipopts.h
|   |   |       |   |   |-- netdb.h
|   |   |       |   |   |-- netif.h
|   |   |       |   |   |-- netifapi.h
|   |   |       |   |   `-- sockets.h
|   |   |       |   `-- lwipopts.h
|   |   |       `-- src
|   |   |           |-- api_shell.c
|   |   |           |-- driverif.c
|   |   |           |-- lwip_init.c
|   |   |           |-- netdb_porting.c
|   |   |           |-- sockets_porting.c
|   |   |           `-- sys_arch.c
|   |   `-- test
|   |       ......
|   |-- power	/* 低功耗模块 */
|   |   |-- BUILD.gn
|   |   |-- los_pm.c
|   |   `-- los_pm.h
|   |-- security	/* 权限管理模块 */
|   |   |-- BUILD.gn
|   |   |-- box
|   |   |   |-- BUILD.gn
|   |   |   |-- los_box.c
|   |   |   `-- los_box.h
|   |   |-- syscall
|   |   |   |-- BUILD.gn
|   |   |   |-- los_syscall.c
|   |   |   |-- los_syscall.h
|   |   |   |-- pthread_syscall.c
|   |   |   `-- syscall_lookup.h
|   |   `-- userlib
|   |       `-- BUILD.gn
|   |-- shell	/* 命令行 */
|   |   |-- BUILD.gn
|   |   |-- Kconfig
|   |   |-- include
|   |   |   |-- shcmd.h
|   |   |   |-- shcmdparse.h
|   |   |   |-- shell.h
|   |   |   |-- shmsg.h
|   |   |   `-- show.h
|   |   `-- src
|   |       |-- base
|   |       |   |-- shcmd.c
|   |       |   |-- shcmdparse.c
|   |       |   |-- shmsg.c
|   |       |   `-- show.c
|   |       `-- cmds
|   |           |-- date_shell.c
|   |           |-- fullpath.c
|   |           |-- mempt_shellcmd.c
|   |           |-- shell_shellcmd.c
|   |           |-- task_shellcmd.c
|   |           `-- vfs_shellcmd.c
|   |-- signal
|   |   |-- BUILD.gn
|   |   |-- Kconfig
|   |   |-- los_signal.c
|   |   `-- los_signal.h
|   `-- trace	/* 调试程序用 */
|       ......
|-- config.gni
|-- config_iccarm.gni
|-- drivers		/* 操作系统四大模块之二：设备管理 */
|   `-- Kconfig
|	/* 具体的外设驱动由芯片原厂在上层文件夹的device和vendor文件夹中提供 */
|-- figures		/* 几张介绍本操作系统内核结构的图片 */
|   ......
|-- kal
|   |-- BUILD.gn
|   |-- Kconfig
|   |-- cmsis	/* ARM格式的中间件统一接口 */
|   |   |-- BUILD.gn
|   |   |-- Kconfig
|   |   |-- cmsis_liteos2.c
|   |   |-- cmsis_os.h
|   |   |-- cmsis_os2.h
|   |   |-- hos_cmsis_adp.h
|   |   `-- kal.h
|   |-- libc	/* C语言标准库，其实现一般都是在编译器中，由芯片原厂完成，所以这里只有头文件 */
|   |   |-- BUILD.gn
|   |   |-- Kconfig
|   |   |-- iccarm
|   |   |   `-- BUILD.gn
|   |   |-- musl
|   |   |   `-- BUILD.gn
|   |   `-- newlib
|   |       |-- BUILD.gn
|   |       `-- porting
|   |           |-- include
|   |           |   |-- arpa
|   |           |   |   `-- inet.h
|   |           |   |-- byteswap.h
|   |           |   |-- dirent.h
|   |           |   |-- endian.h
|   |           |   |-- ifaddrs.h
|   |           |   |-- limits.h
|   |           |   |-- malloc.h
|   |           |   |-- mqueue.h
|   |           |   |-- net
|   |           |   |   |-- ethernet.h
|   |           |   |   |-- if.h
|   |           |   |   `-- if_arp.h
|   |           |   |-- netdb.h
|   |           |   |-- netinet
|   |           |   |   |-- if_ether.h
|   |           |   |   |-- in.h
|   |           |   |   |-- ip.h
|   |           |   |   `-- tcp.h
|   |           |   |-- poll.h
|   |           |   |-- semaphore.h
|   |           |   |-- sys
|   |           |   |   |-- _pthreadtypes.h
|   |           |   |   |-- fcntl.h
|   |           |   |   |-- features.h
|   |           |   |   |-- ioctl.h
|   |           |   |   |-- mount.h
|   |           |   |   |-- prctl.h
|   |           |   |   |-- sched.h
|   |           |   |   |-- select.h
|   |           |   |   |-- socket.h
|   |           |   |   |-- statfs.h
|   |           |   |   |-- uio.h
|   |           |   |   `-- un.h
|   |           |   `-- time.h
|   |           `-- src
|   |               |-- hook_adapt.c
|   |               `-- network
|   |                   |-- htonl.c
|   |                   |-- htons.c
|   |                   |-- ntohl.c
|   |                   `-- ntohs.c
|   |-- libsec
|   |   `-- BUILD.gn
|   `-- posix	/* 操作系统给应用暴露出来的通用接口 */
|       |-- BUILD.gn
|       |-- Kconfig
|       |-- include
|       |   |-- libc.h
|       |   |-- pipe_impl.h
|       |   |-- poll_impl.h
|       |   `-- rtc_time_hook.h
|       `-- src
|           |-- errno.c
|           |-- libc.c
|           |-- libc_config.h
|           |-- malloc.c	/* 操作系统的四大模块之三：内存管理 */
|           |-- map_error.c
|           |-- map_error.h
|           |-- mqueue.c
|           |-- mqueue_impl.h
|           |-- pipe.c
|           |-- poll.c
|           |-- pthread.c
|           |-- pthread_attr.c
|           |-- pthread_cond.c
|           |-- pthread_mutex.c
|           |-- semaphore.c
|           |-- signal.c
|           |-- time.c
|           `-- time_internal.h
|-- kernel	/* 操作系统的四大模块之四：进程管理 */
|   |-- BUILD.gn
|   |-- include
|   |   |-- los_config.h
|   |   |-- los_event.h
|   |   |-- los_membox.h
|   |   |-- los_memory.h
|   |   |-- los_mux.h
|   |   |-- los_queue.h
|   |   |-- los_sched.h
|   |   |-- los_sem.h
|   |   |-- los_sortlink.h
|   |   |-- los_swtmr.h
|   |   |-- los_task.h
|   |   `-- los_tick.h
|   `-- src
|       |-- los_event.c
|       |-- los_init.c
|       |-- los_mux.c
|       |-- los_queue.c
|       |-- los_sched.c
|       |-- los_sem.c
|       |-- los_sortlink.c
|       |-- los_swtmr.c
|       |-- los_task.c
|       |-- los_tick.c
|       `-- mm
|           |-- los_membox.c
|           `-- los_memory.c
|-- liteos.gni
|-- testsuites	/* 移植系统后进行自测用的，不用关心 */
|   ......
|-- tools
|   `-- mem_analysis.py
`-- utils
    |-- BUILD.gn
    |-- internal
    |   |-- los_hook_types.h
    |   `-- los_hook_types_parse.h
    |-- los_compiler.h
    |-- los_debug.c
    |-- los_debug.h	/* 串口调试输出的级别 */
    |-- los_error.c
    |-- los_error.h	/* 所有模块都会用的，要返回的错误码 */
    |-- los_hook.c
    |-- los_hook.h
    |-- los_list.h	/* 链表，队列、模块缓存的基础 */
    `-- los_reg.h

198 directories, 1571 files
```

* 基础的操作系统内核代码里没有太多可借鉴的软件结构，它们都是针对某一项功能而实现一项功能，通用的东西较少，接下来我会从操作系统移植的角度稍微分析一下。